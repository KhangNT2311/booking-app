import React from 'react'
import { Link } from 'react-router-dom'
import { BRAND_NAME } from 'src/constants/commons'
import RoutesString from 'src/routes/routesString'

function PaymentSuccess() {
    return (
        <div>
            <div className="jumbotron text-center">
                <h1 className="display-3 pb-5">Thank You!</h1>
                <p class="lead"><strong>Đặt phòng thành công</strong>, cảm ơn bạn đã đồng hành cùng {BRAND_NAME}</p>
                <hr />
                <p className='pb-5'>
                    Nếu gặp bất cứ vấn đề gì, <a href="">hãy liên hệ chúng tôi</a>
                </p>
                <p className="lead">
                    <Link to={RoutesString.HOME} class="btn btn-primary btn-sm" href="https://bootstrapcreative.com/" role="button">Tiếp tục xem phòng nào</Link>
                </p>
            </div>
        </div>
    )
}

export default PaymentSuccess
