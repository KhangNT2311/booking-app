import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import CircularProgress from '@mui/material/CircularProgress';
import { BRAND_NAME } from 'src/constants/commons';
import { getHomeById } from 'src/store/slices/clientSlice';
import './RoomPage.scss';
import { useEffect } from 'react';
import { numberWithCommas } from 'src/utils/helper';
import Slider from "react-slick";
import Circle from 'src/components/atoms/circle/Circle';
import RoutesString from 'src/routes/routesString';

function RoomPage() {
  const { id } = useParams();
  const { push } = useHistory();
  const dispatch = useDispatch();
  const isLoading = useSelector(state => state.client.currentHomeLoading);
  const home = useSelector(state => state.client.home);
  const { homeName,
    homeDescription,
    homeIntro,
    homeSize,
    homeImages,
    district,
    ward,
    street,
    apartmentNumber,
    homeRule,
    bathrooms,
    bedrooms,
    kitchens,
    province,
    homeType,
    owner,
    status,
    homeFees } = home
  useEffect(() => {
    dispatch(getHomeById(id))
  }, [])
  const handleBooking = () => {
    push(RoutesString.CHECKOUT_INFORMATION.replace(":id", id))
  }
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
  };
  const nameArr = owner?.name?.split(" ") || []
  const firstLetter = nameArr[nameArr?.length - 1]?.[0];
  const dayOfWeek = new Date().getDay();
  const isWeekend = (dayOfWeek === 6) || (dayOfWeek === 0);
  const renderItems = (items) =>
    items.map((item, idx) => (
      <div key={`home-img-${idx}`} className="homeHighlight__slide cursor-pointer" >
        <div>
          <img className='h-96 w-100' src={item} alt="home" />
        </div>
      </div>
    ));
  return <div className='roomPage'>
    {isLoading ? <div className='d-flex justify-center h-screen items-center'>
      <CircularProgress />
    </div>
      : <div className="detail">
        <div className="container container_room">
          <div className="section">
            <div className="row">
              <div>
                <Slider {...settings}>{renderItems(homeImages)}</Slider>
              </div>
              <div className="col-md-8 col-xs-12">
                <div className="detail_left">
                  <div className="section">
                    <div className="breadcrumbs">
                      <a href="#">{BRAND_NAME}</a>
                      <span className="is-flex middle-xs">
                        <a href="#">{province}</a>
                      </span>
                      <span className="is-flex middle-xs">
                        <a href="#">{district}</a>
                      </span>
                      <span className="is-flex middle-xs">
                        <a href="#">{ward}</a>
                      </span>
                    </div>
                    <div className="row">
                      <div className="col-xs-12">
                        <div>
                          <div className="title">
                            <div className="row">
                              <div className="col-xs-12 col-sm-10">
                                <div className="title_start">
                                  {homeName}
                                </div>
                              </div>
                              <div className="col-lg-2 room_id">
                                <div className="image_box">
                                  <Circle src={owner.acatar} letter={firstLetter} className="w-10 h-10" />
                                  {/* <img src={owner.avatar} alt="avatar" /> */}
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="detail_location ">
                            <div className="detail_flex">
                              <i class="fas fa-map-marker-alt"></i>
                              <div className="ml--6 text_bold">{`${province}, ${district}, ${ward}`} </div>
                            </div>
                          </div>
                          <div className="mt--12">
                            <div className="detail_flex">
                              <i class="fas fa-building"></i>
                              <div className="ml--6 text_bold">{homeType?.homeTypeName}</div>
                              <div className="span">
                                · {homeSize}
                                <sup>2</sup>
                              </div>
                            </div>
                          </div>
                          <div className="detail_type">
                            <div className="detail_flex">
                              <div className="mb--0">
                                <span>{bathrooms} Phòng tắm  ·  </span>
                                <span>{bedrooms} phòng ngủ  ·  </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="detail_intro" dangerouslySetInnerHTML={{ __html: homeDescription }} >

                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <div className="title mt--60">
                    <div className="title_room1">Tiện nghi chỗ ở</div>
                    <span>Giới thiệu về các tiện nghi và dịch vụ tại nơi lưu trú</span>
                  </div> */}
                  {/* <div className="amenities">
                    <div className="item_room">
                      <div className="item_room1">
                        <div>
                          <div className="mt--24 mb--0 title_amenities">Tiện ích</div>
                          <ul className="list_room">
                            <li className="mt--12">
                              <i class="fas fa-wifi"></i>
                              <div className="list_title">Wifi</div>
                            </li>
                            <li className="mt--12">
                              <i class="fas fa-tv"></i>
                              <div className="list_title">TV</div>
                            </li>
                            <li className="mt--12">
                              <i class="fas fa-temperature-low"></i>
                              <div className="list_title">Điều hòa</div>
                            </li>
                            <li className="mt--12">
                              <i class="fas fa-pump-soap"></i>
                              <div className="list_title">Dầu gội,dầu xả</div>
                            </li>
                            <li className="mt--12">
                              <i class="fas fa-toilet-paper"></i>
                              <div className="list_title">Giấy vệ sinh</div>
                            </li>
                            <li className="mt--12">
                              <i class="fas fa-soap"></i>
                              <div className="list_title">Xà phòng tắm</div>
                            </li>
                            <li className="mt--12">
                              <i class="fas fa-fan"></i>
                              <div className="list_title">Máy sấy</div>
                            </li>
                            <li className="mt--12">
                              <i class="fas fa-broadcast-tower"></i>
                              <div className="list_title">Internet</div>
                            </li>
                          </ul>
                        </div>
                        <div>
                          <div className="mt--24 mb--0 title_amenities">Tiện ích bếp</div>
                          <ul className="list_room">
                            <li className="mt--12">
                              <i class="fas fa-lightbulb"></i>
                              <div className="list_title">Bếp điện</div>
                            </li>
                            <li className="mt--12">
                              <i class="fas fa-fire"></i>
                              <div className="list_title">Lò vi sóng </div>
                            </li>
                            <li className="mt--12">
                              <i class="far fa-snowflake"></i>
                              <div className="list_title">Tủ lạnh</div>
                            </li>
                          </ul>
                        </div>
                        <div>
                          <div className="mt--24 mb--0 title_amenities">Tiện ích phòng</div>
                          <ul className="list_room">
                            <li className="mt--12">
                              <i class="fab fa-windows"></i>
                              <div className="list_title">Cửa sổ</div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div> */}
                  <div>
                    <div className="room_title">
                      <div className="mb--6"> Giá phòng </div>
                      <span>Giá có thể tăng vào cuối tuần hoặc ngày lễ</span>
                    </div>
                    <div className="room_price">
                      <div className="price_blur">
                        <span className="rice_left">Thứ hai - Thứ sáu</span>
                        <span className="rice_right">{numberWithCommas(homeFees?.[0]?.price)}<sup>đ</sup> </span>
                      </div>
                      <div className="price_w">
                        <span className="rice_left">Thứ bảy - Chủ nhật</span>
                        <span className="rice_right">{numberWithCommas(homeFees?.[1]?.price)}<sup>đ</sup> </span>
                      </div>
                    </div>
                  </div>

                  <div className="mt--60">
                    <div className="mb--6">Nội quy về chính sách và chỗ ở</div>
                  </div>
                  <div dangerouslySetInnerHTML={{ __html: homeRule }}>

                  </div>
                </div>
              </div>
              <div className="col-md-4 col-xs-12">
                <div className="detail_right">
                  <button className="detail_share">
                    <span className="pr--6">Chia sẻ</span>
                    <i class="far fa-share-square"></i>
                  </button>
                  <button className="detail_like">
                    <span className="pr--6">Lưu lại</span>
                    <i class="far fa-heart"></i>
                  </button>
                </div>
                <div className="room_sidebar">
                  <div className="sidebar_content mb--18">
                    <div className="sidebar1 px--lg--24">
                      <div className="sidebar_pricing">
                        <div className="sidebar2">
                          <span className="pricing_bold">{isWeekend ? numberWithCommas(homeFees?.[1]?.price) : numberWithCommas(homeFees?.[0]?.price)}</span>
                          <span className="night"> /đêm</span>
                        </div>
                      </div>
                      {/* <div className="room_discount">
                        <span className="lable_discount">Giảm 30% từ chủ nhà</span>
                        <p className="text_discount">
                          Giảm
                          <strong> 30% </strong>
                          cho đặt phòng có checkin từ
                          <strong> 15/11 </strong>
                          đến
                          <strong> 31/01/22 </strong>
                        </p>
                      </div> */}
                      <div className="">

                      </div>
                      <div className="input-group mt--6">
                        <div className="modal_booking">
                          <p> 1 khách </p>
                        </div>
                      </div>
                      <button onClick={handleBooking} className="btn btn_room">
                        <i class="fas fa-bolt"></i>
                        <span> Đặt ngay </span>
                      </button>
                      <div></div>
                    </div>
                  </div>
                  <div className="sidebar_content">
                    <div className="py--30 px--12 px--lg--24">
                      <div>
                        <h3 className="advise_title">Tư vấn từ {BRAND_NAME}</h3>
                        <p>
                          Vui lòng cung cấp số điện thoại để nhận được tư vấn từ {BRAND_NAME} cho chuyến đi của bạn.
                        </p>
                      </div>
                      <div>
                        <div className="input_custom">
                          <input placeholder="Tên Khách hàng" aria-required="true" aria-invalid="false" className="input"></input>
                        </div>
                        <div className="input_custom">
                          <input placeholder="Số điện thoại" aria-required="true" aria-invalid="false" className="input"></input>
                        </div>
                        <div className="mb--30">
                          <button className="btn btn_black"> Nhận tư vấn miễn phí </button>
                        </div>
                      </div>
                      <div></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>}

  </div>;
}

export default RoomPage;
