import { useFormik } from 'formik'
import React from 'react'
import { useSelector } from 'react-redux';
import { Input } from 'reactstrap';
import InputField from 'src/components/atoms/input/InputField';
import Dropzone from 'src/components/molecules/dropzone/Dropzone';
import moment from 'moment';
import './ClientSettingsForm.scss';
import { useState } from 'react';
import { useEffect } from 'react';
const init = {
    name: "",
    email: "",
    phoneNumber: "",
    gender: "",
    identification: {
        fullName: "",
        idCardNumber: "",
        issuedAt: "",
        images: [],
    }
};
function ClientSettingsForm({ handleSubmit }) {
    const user = useSelector((state) => state.auth.user);
    const [previewImages, setPreviewImages] = useState([]);
    useEffect(() => {
        setPreviewImages(user?.identification?.images || [])
    }, [user])

    const { setFieldValue, handleBlur, handleChange, handleSubmit: onSubmit, values, errors, touched } = useFormik({
        initialValues: (() => {
            const { name, email, phoneNumber, gender, identification } = user || init;
            const { fullName, idCardNumber, issuedAt, images } = identification || init.identification
            return { name, email, phoneNumber, gender, fullName, idCardNumber, images, issuedAt, newImages: [] }
        })(),
        onSubmit: (values) => {
            const { fullName, idCardNumber, images, issuedAt, ...updatedValues } = values;
            updatedValues.identification = { fullName, idCardNumber, images, issuedAt }
            handleSubmit(updatedValues);
        }
    })
    const handleUpload = (file) => {
        if (file) {
            setPreviewImages([...previewImages, URL.createObjectURL(file)]);
            setFieldValue("newImages", [...values.newImages, file])
        }
    }
    const fields = [{
        label: "ĐỊA CHỈ EMAIL",
        required: true,
        type: "email",
        name: "email",
        col: 6,
        placeholder: "",
        readOnly: true
    }, {
        label: "HỌ VÀ TÊN",
        required: true,
        type: "text",
        name: "name",
        col: 6,
        placeholder: ""
    }, {
        label: "SỐ ĐIỆN THOẠI",
        required: true,
        type: "text",
        name: "phoneNumber",
        col: 6,
        placeholder: "",
    }, {
        label: "GIỚI TÍNH",
        type: "select",
        name: "gender",
        col: 6,
        placeholder: "",
        options: [{
            value: "Nam",
            label: "Nam"
        }, {
            value: "Nữ",
            label: "Nữ"
        }, {
            value: "Khác",
            label: "Khác"
        }]
    }, {
        label: "NGÀY SINH",
        type: "date",
        name: "birthday",
        col: 6,
        placeholder: "",
    }]
    const indentityFields = [{
        label: "HỌ TÊN ĐẦY ĐỦ",
        required: true,
        type: "text",
        name: "fullName",
        col: 6,
        placeholder: ""
    }, {
        label: "SỐ CMND/CĂN CƯỚC",
        required: true,
        type: "text",
        name: "idCardNumber",
        col: 6,
        placeholder: ""
    }, {
        label: "NGÀY CẤP",
        required: true,
        type: "date",
        name: "issuedAt",
        col: 12,
        placeholder: "Ngày cấp"
    }]

    const renderFields = (fields) => fields.map(({ col, type, name, label, placeholder, ...props }, idx) => {
        const value = values[name];
        const error = errors[name];
        const touch = touched[name];
        const updatedValue = type === 'date' ? moment(value).format("YYYY-MM-DD") : value;
        return <div key={`${name}-${idx}`} className={`mt-3 col-${col}`}>
            <InputField label={label}
                placeholder={placeholder}
                id={`${name}Id`}
                name={name}
                value={updatedValue}
                onChange={handleChange}
                onBlur={handleBlur}
                type={type}
                {...props}
            />
        </div>
    })

    return (
        <form onSubmit={onSubmit}>
            <div className='row'>
                {renderFields(fields)}
                <p className='mt-3'>CMND/CĂN CƯỚC</p>
                {renderFields(indentityFields)}
                <p className='text-xs font-medium text-gray-700 mt-3 mb-2'>ẢNH CMND/CĂN CƯỚC</p>
                <div className='d-flex flex-wrap'>
                    {previewImages && previewImages.map((previewImage) => (
                        <div className='previewImage'>
                            <img className='w-full h-full' src={previewImage} />
                        </div>
                    ))}
                    <div>
                        <Dropzone isPreview={false} handleUpload={handleUpload} />
                    </div>
                </div>
                <button className='mt-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded'>
                    Lưu cài đặt
                </button>
            </div>
        </form>
    )
}

export default ClientSettingsForm
