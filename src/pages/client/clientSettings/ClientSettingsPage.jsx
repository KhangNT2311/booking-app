import React, { useEffect, useRef } from 'react'
import { TiCameraOutline } from 'react-icons/ti'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import Circle from 'src/components/atoms/circle/Circle'
import './ClientSettingsPage.scss'
import RoutesString from 'src/routes/routesString'
import { getSecureS3Url, uploadS3BucketImage } from 'src/services/commons'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { getProfile, updateProfile } from 'src/store/slices/authSlice'
import ClientSettingsForm from './form/ClientSettingsForm'

function ClientSettingsPage() {
    const user = useSelector(state => state.auth?.user)
    const inputFileRef = useRef(null);
    const [avatar, setAvatar] = useState(user?.avatar || "")
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getProfile())
    }, [])
    const handleSubmit = async (values) => {
        const { newImages, ...updatedValues } = values;
        if (newImages.length > 0) {
            let promises = [];
            const uploadImage = async (file) => {
                const secureUrl = await getSecureS3Url();
                return await uploadS3BucketImage(secureUrl, file)
            }
            newImages.forEach((file) => {
                promises.push(uploadImage(file))
            })
            const response = await Promise.all(promises);
            updatedValues.identification.images = response;

        }
        dispatch(updateProfile(updatedValues))
    }
    const handleOpenFile = () => {
        if (inputFileRef.current) {
            inputFileRef.current.click();
        }
    }
    const handleUploadImage = async (e) => {
        const { files } = e.target;
        const secureUrl = await getSecureS3Url();
        const imageUrl = await uploadS3BucketImage(secureUrl, files?.[0])
        setAvatar(imageUrl);
        dispatch(updateProfile({ avatar: imageUrl }))
    }
    console.log("user",user);
    return (
        <div className='container-fluid'>
            <h2 className='font-bold text-xl pb-2'>Cài đặt tài khoản</h2>
            <p className='mb-2'><strong>{user?.name}</strong>, {user?.email}</p>
            <div className='grid grid-cols-12'>
                <div className='col-span-3'>
                    <div className='w-full border-gray-400 border pt-4'>
                        <div className='border-b border-gray-400 pb-4 px-4'>
                            <div className='relative d-flex justify-center'>
                                <Circle letter="NTK" src={avatar} className="w-28 h-28 text-2xl" />
                                <div onClick={handleOpenFile} className='text-white p-2 rounded bg-gray-900 bg-opacity-50 d-flex absolute bottom-0 right-5 items-center cursor-pointer'>
                                    <TiCameraOutline className='mr-2' />
                                    <span>Chỉnh sửa</span>
                                </div>
                                <input className='hidden' onChange={handleUploadImage} ref={inputFileRef} type="file" />
                            </div>
                            <h2 className='text-2xl font-bold mt-4'>
                                {user?.name}
                            </h2>
                            <p>
                                {user?.email}
                            </p>
                        </div>
                        {/* <div className='py-4'>
                            <p className='px-4'>THÔNG TIN CHI TIẾT</p>
                            <NavLink className="px-4 py-2 my-1 w-full block hover:text-primary" to={RoutesString.HOST_ACCOUNT_SETTINGS} activeClassName='host_active'>Thông tin cá nhân</NavLink>

                        </div> */}
                    </div>
                </div>
                <div className="col-span-9 ml-4 pt-4">
                    <div className='pb-3 '>
                        <h2 className='font-bold text-xl'>Thông tin cá nhân</h2>
                        <p className='text-gray-600'>Cá nhân hóa tài khoản bằng việc cập nhật thông tin của bạn</p>
                    </div>
                    <div className='pb-5 pt-3 border-t border-gray-300 border-b'>
                        <ClientSettingsForm handleSubmit={handleSubmit} />
                    </div>
                </div>
            </div>

        </div>
    )
}

export default ClientSettingsPage
