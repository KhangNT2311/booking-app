import React from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import Card from "src/components/molecules/card/Card";
import { getListHomes } from "src/store/slices/clientSlice";
import CircularProgress from '@mui/material/CircularProgress';

import "./LocationPage.scss";
import RoutesString from "src/routes/routesString";
function LocationPage() {
  const dispatch = useDispatch();
  const { push } = useHistory();
  const { province } = useParams();
  const homes = useSelector(state => state.client.homes);
  const homeLoading = useSelector(state => state.client.homeLoading);
  useEffect(() => {
    dispatch(getListHomes(province))
  }, [])
  const handleClick = (id) => () => {
    push(RoutesString.ROOM.replace(":id", id))
  }
  return (
    <div class="Container container--md">
      <div class="el-row">
        <div class="el-col-md-24">
          <div class="section">
            <div class="row">
              <div class="title col-xs-12 col-md-9">
                {province}
              </div>
              <div class="col-xs-12 col-md-3 is-flex jend">
                {/* <div class="select">
                  <div class="input_select">
                    <select name="choice" id="choice" className="select1">
                      <option value="">Giá tăng dần</option>
                      <option value="">Giá giảm dần</option>
                    </select>
                  </div>
                </div> */}
              </div>
            </div>
            {homeLoading ?
              <div className="d-flex justify-center items-center w-full h-screen">
                <CircularProgress />
              </div>


              : <div class="row">
                {homes && homes.map((item) => {
                  return <Card onClick={handleClick(item?._id)} src={item?.homeImages?.[0]} price={item?.homeFees?.[0]?.price} title={item.homeName} description={item.homeIntro} />
                })}
              </div>}
          </div>
        </div>
      </div>
    </div>
  );
}

export default LocationPage;
