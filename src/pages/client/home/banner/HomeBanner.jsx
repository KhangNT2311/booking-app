import React from "react";
import Slider from "react-slick";
import "./HomeBanner.scss";
const HomeBanner = ({ data }) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  const renderSliderItem = (items) =>
    items.map((item, idx) => (
      <div key={`home-banner-${idx}`} className="homeBanner__item">
        <img src={item.src} alt={`banner-${idx}`} />
      </div>
    ));
  return (
    <div className="homeBanner">
      <div className="homeBanner__wrapper">
        <Slider {...settings}>{renderSliderItem(data)}</Slider>
      </div>
    </div>
  );
};

export default HomeBanner;
