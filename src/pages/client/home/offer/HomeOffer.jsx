import React from "react";
import { BRAND_NAME } from "src/constants/commons";
import "./HomeOffer.scss";
const HomeOffer = ({ data }) => {
  const renderSliderItem = (items) =>
    items.map((item, idx) => (
      <div key={`home-banner-${idx}`} className="homeOffer__item">
        <img src={item.src} alt={`banner-${idx}`} />
      </div>
    ));
  return (
    <div className="homeOffer">
      <div className="homeOffer__wrapper">
        <h2 className="text-2xl font-bold pb-2"> Ưu đãi độc quyền</h2>
        <p>Chỉ có tại {BRAND_NAME}, hấp dẫn và hữu hạn, book ngay!</p>
        <div className="homeOffer__items">{renderSliderItem(data)}</div>
      </div>
    </div>
  );
};

export default HomeOffer;
