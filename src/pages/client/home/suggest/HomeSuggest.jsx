import React from "react";
import Slider from "react-slick";
import SimpleCard from "src/components/molecules/SimpleCard/SimpleCard";
import { BRAND_NAME } from "src/constants/commons";
import "./HomeSuggest.scss";
const HomeSuggest = ({ data }) => {
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
  };
  const renderItems = (items) =>
    items.map((item, idx) => (
      <SimpleCard
        {...item}
        key={`home-suggest-${idx}`}
        className="homeOffer__item"
      />
    ));
  return (
    <div className="homeSuggest">
      <div className="homeSuggest__wrapper">
        <h2 className="text-2xl font-bold pb-2">Gợi ý từ {BRAND_NAME}</h2>
        <p>Những địa điểm thường đến mà {BRAND_NAME} gợi ý dành cho bạn</p>
        <Slider {...settings} className="homeSuggest__items">
          {renderItems(data)}
        </Slider>
      </div>
    </div>
  );
};

export default HomeSuggest;
