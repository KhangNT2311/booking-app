import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { BRAND_NAME } from 'src/constants/commons';
import RoutesString from 'src/routes/routesString';
import './HomeIntroduction.scss';
const HomeIntroduction = () => {
  const user = useSelector(state => state.auth.user);
  return (
    <div className='homeIntroduction'>
      <div className='homeIntroduction__wrapper'>
        <h2 className='text-3xl font-bold pb-2'>Chào mừng bạn đến với {BRAND_NAME}!</h2>
        <p>
          Đặt chỗ ở, homestay, cho thuê xe, trải nghiệm và nhiều hơn thế nữa
          trên {BRAND_NAME}
        </p>
        {!user && <p>
          <Link to={RoutesString.LOGIN}>Đăng nhập</Link> hoặc{' '}
          <Link to={RoutesString.SIGN_UP}>Đăng ký</Link> để trải nghiệm{' '}
        </p>}

      </div>
    </div>
  );
};

export default HomeIntroduction;
