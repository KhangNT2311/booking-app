import React from 'react';
import HomeBanner from './banner/HomeBanner';
import './HomePage.scss';
import banner1 from '../../../assets/images/home/banner-1.png';
import banner2 from '../../../assets/images/home/banner-2.png';
import highlight1 from '../../../assets/images/home/highlight-1.png';
import highlight2 from '../../../assets/images/home/highlight-2.png';
import highlight3 from '../../../assets/images/home/highlight-3.png';
import highlight4 from '../../../assets/images/home/highlight-4.png';
import highlight5 from '../../../assets/images/home/highlight-5.png';
import highlight6 from '../../../assets/images/home/highlight-6.png';
import offer1 from '../../../assets/images/home/offer-1.jpg';
import offer2 from '../../../assets/images/home/offer-2.jpg';
import offer3 from '../../../assets/images/home/offer-3.jpg';
import suggest1 from '../../../assets/images/home/suggest-1.jpg';
import suggest2 from '../../../assets/images/home/suggest-2.jpg';
import suggest3 from '../../../assets/images/home/suggest-3.jpg';
import suggest4 from '../../../assets/images/home/suggest-4.jpg';
import suggest5 from '../../../assets/images/home/suggest-5.jpg';
import HomeIntroduction from './introduction/HomeIntroduction';
import HomeHighlight from './highlight/HomeHighlight';
import HomeOffer from './offer/HomeOffer';
import HomeSuggest from './suggest/HomeSuggest';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { getListProvinces } from 'src/store/slices/clientSlice';
import { useSelector } from 'react-redux';
const HomePage = () => {
  const dispatch = useDispatch()
  const provinces = useSelector(state => state.client.provinces);

  const bannerData = [
    {
      src: banner1,
    },
    {
      src: banner2,
    },
  ];
  const highlightImgs = [highlight1, highlight2, highlight3, highlight4, highlight5, highlight6];
  const highlightData = provinces.map((pro, idx) => ({
    title: pro,
    src: highlightImgs[idx]
  }))
  // const highlightData = [
  //   {
  //     title: 'Vũng Tàu',
  //     src: highlight1,
  //     quantity: 549,
  //   },
  //   {
  //     title: 'Đà Lạt',
  //     src: highlight2,
  //     quantity: 1491,
  //   },
  //   {
  //     title: 'Đà Nẵng',
  //     src: highlight3,
  //     quantity: 897,
  //   },
  //   {
  //     title: 'Nha Trang',
  //     src: highlight4,
  //     quantity: 637,
  //   },
  //   {
  //     title: 'Quảng Ninh',
  //     src: highlight5,
  //     quantity: 219,
  //   },
  //   {
  //     title: 'Hội An',
  //     src: highlight6,
  //     quantity: 323,
  //   },
  // ];
  const offerData = [
    {
      src: offer1,
    },
    {
      src: offer2,
    },
    {
      src: offer3,
    },
  ];
  const suggestData = [
    {
      title: 'Hà Nội nội thành lãng mạn',
      description: 'Không gian lãng mạn dành cho cặp đôi tại trung tâm Hà Nội',
      src: suggest1,
    },
    {
      title: 'Hà Nội nội thành lãng mạn',
      description: 'Không gian lãng mạn dành cho cặp đôi tại trung tâm Hà Nội',
      src: suggest2,
    },
    {
      title: 'Hà Nội nội thành lãng mạn',
      description: 'Không gian lãng mạn dành cho cặp đôi tại trung tâm Hà Nội',
      src: suggest3,
    },
    {
      title: 'Hà Nội nội thành lãng mạn',
      description: 'Không gian lãng mạn dành cho cặp đôi tại trung tâm Hà Nội',
      src: suggest4,
    },
    {
      title: 'Hà Nội nội thành lãng mạn',
      description: 'Không gian lãng mạn dành cho cặp đôi tại trung tâm Hà Nội',
      src: suggest5,
    },
  ];
  const homeBannerProps = {
    data: bannerData,
  };
  const homeHighlightProps = {
    data: highlightData,
  };
  const homeOfferProps = {
    data: offerData,
  };
  const homeSuggestProps = {
    data: suggestData,
  };
  useEffect(() => {
    dispatch(getListProvinces)
  }, [])
  return (
    <div className='homePage'>
      <div className='homePage__wrapper'>
        <div className='container'>
          <HomeBanner {...homeBannerProps} />
          <HomeIntroduction />
          <HomeHighlight {...homeHighlightProps} />
          <HomeOffer {...homeOfferProps} />
          <HomeSuggest {...homeSuggestProps} />
        </div>
      </div>
    </div>
  );
};

export default HomePage;
