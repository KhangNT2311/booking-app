import React from "react";
import { useHistory } from "react-router-dom";
import Slider from "react-slick";
import { BRAND_NAME } from "src/constants/commons";
import RoutesString from "src/routes/routesString";
import "./HomeHighlight.scss";
const HomeHighlight = ({ data }) => {
  const { push } = useHistory();
  let dragging = true;
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    beforeChange: () => dragging = true,
    afterChange: () => dragging = false
  };
  const handleOnDragStart = (e) =>
    e.preventDefault()
  let moved = false;
  const downListener = () => {
    moved = false
  }
  const moveListener = () => {
    moved = true
  }
  const upListener = (name) => () => {
    if (!moved) {
      name && push(RoutesString.LOCATION.replace(":province", name))
    }
  }
  // const handleClick = (name) => () => {
  //   dragging && 
  // }
  const renderItems = (items) =>
    items.map((item) => (
      <div
        onMouseDown={downListener}
        onMouseMove={moveListener}
        onMouseUp={upListener(item.title)}
        className="homeHighlight__slide cursor-pointer">
        <div className="homeHighlight__item">
          <img src={item.src} alt={item.title} />
          <div className="homeHighlight__item__overlay">
            <h4 className=" font-bold text-white" >{item.title}</h4>
          </div>
        </div>
      </div>
    ));
  return (
    <div className="homeHighlight">
      <div className="homeHighlight__wrapper">
        <h2 className="text-2xl font-bold pb-2">Địa điểm nổi bật</h2>
        <p>
          Cùng {BRAND_NAME} bắt đầu chuyến hành trình chinh phục thế giới của bạn
        </p>
        <Slider {...settings}>{renderItems(data)}</Slider>
      </div>
    </div>
  );
};

export default HomeHighlight;
