import React, { useState } from "react";
import axios from 'axios';
import { useFormik } from "formik";
import * as Yup from 'yup'
import { BRAND_NAME } from "src/constants/commons";
import RoutesString from "src/routes/routesString";
import { Link } from "react-router-dom";
function LoginForm({ handleSubmit }) {

  const { handleBlur, handleChange, handleSubmit: onSubmit, values, errors, touched } = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object().shape({
      email: Yup.string().email("Invalid email").required("Required"),
      password: Yup.string().required("Required").min(4, "Min 4"),
    }),
    onSubmit: (values) => {
      handleSubmit(values);
    }
  })

  return (
    <div class="login">
      <div>
        <div class="login_title">Đăng nhập</div>
        <div class="login_title1">
          <label class="login_group"> Đăng nhập {BRAND_NAME} để trải nghiệm</label>
        </div>
        <form class="login_body" onSubmit={onSubmit}>
          <div class="input_login is-relative">
            <input
              name="email"
              id="email"
              placeholder="Địa chỉ Email"
              autoComplete="username"
              data-vv-as="Email"
              class="input"
              aria-required="true"
              value={values.email}
              //value={this.state.email}
              onChange={handleChange}
              onBlur={handleBlur}
            />
            <span class="is-absolute input_icon">
              <i class="far fa-envelope"></i>
            </span>
          </div>
          <div class="input_login is-relative">
            <input
              type="password"
              name="password"
              id="password"
              placeholder="Mật khẩu"
              autoComplete="password"
              class="input"
              value={values.password}
              onChange={handleChange}
              onBlur={handleBlur}
            />
            <span class="is-absolute input_icon">
              <img
                class="img2"
                src="https://www.luxstay.com/icons/locked.svg"
                width="14"
              ></img>
            </span>
          </div>
          <button type="submit" class="btn" value="Submit">
            Đăng nhập
          </button>
          <div class="login_center">
            <div>
              Quên mật khẩu ?{" "}
              <a href="" class="colortext">
                Nhấn vào đây
              </a>
            </div>
            <div class="register">
              Bạn chưa có tài khoản {BRAND_NAME} ?
              <Link to={RoutesString.SIGN_UP} class="cursor-pointer colortext">
                Đăng ký
              </Link>
            </div>
          </div>
        </form>
        {/* <div class="login_body">
          <div class="login_center">Hoặc</div>
          <div class="login_social">
            <div>
              <div class="btn_social">
                <a href="#" class="text3">
                  {" "}
                  Đăng nhập với Facebook
                </a>
                <i class="icon-facebook-square"></i>
              </div>
            </div>
            <div>
              <div class="btn_social">
                <a href="#" class="text3">
                  {" "}
                  Đăng nhập với Google
                </a>
                <i class="icon-facebook-square"></i>
              </div>
            </div>
          </div>
        </div> */}
      </div>
    </div>
  );
}

export default LoginForm;
