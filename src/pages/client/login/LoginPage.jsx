import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { useHistory } from 'react-router';
import { BRAND_NAME } from 'src/constants/commons';
import { USER_ROLE } from 'src/constants/enums';
import { login, register } from 'src/store/slices/authSlice';
import { useQueryParam, BooleanParam } from 'use-query-params';
import LoginForm from './loginForm/LoginForm';
import './LoginPage.scss';
import SignUpForm from './signUpForm/SignUpForm';
const LoginPage = () => {
  const [isSignUp, setIsSignUp] = useState(false);
  const { search } = useLocation();
  const dispatch = useDispatch();
  let searchParams = new URLSearchParams(search);
  const host = searchParams.get('host');
  useEffect(() => {
    let searchParams = new URLSearchParams(search);
    const isSignUp = searchParams.get('isSignUp');
    setIsSignUp(isSignUp);
  }, [search]);

  const handleSubmit = (values) => {
    const { firstName, lastName, confirmPassword, ...updatedValues } = values
    updatedValues.name = `${firstName} ${lastName}`;
    let searchParams = new URLSearchParams(search);
    const host = searchParams.get('host');
    if (host) {
      updatedValues.role = USER_ROLE.HOST
    }
    register(updatedValues)(dispatch)
  }
  const handleLogin = (values) => {
    login(values)(dispatch)
  }
  return (
    <body>
      <div class='welcome'>
        <div class='container-fluid'>
          <div class='row'>
            <div class='col-md-7 col-xs-12 '>
              <h1 class='welcome_title'>
                Đăng ký thành viên {BRAND_NAME} - Tích điểm thưởng và nhận ưu đãi
              </h1>
              <p class='welcome_title1'>
                Nhanh chóng , tiện lợi và an toàn. Đăng ký liền tay, rinh ngay
                quyền lợi.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class='section'>
        <div class='container-fluid'>
          <div class='row'>
            <div class='col-md-8 col-xs-12 last-xs first-md'>
              <div class='row'>
                <div class='col-md-6 col-sm-6 col-xs-12'>
                  <div class='media'>
                    <img
                      class='img1'
                      src='https://www.luxstay.com/account/coins@2x.png'
                      width='65'
                      height='70'
                    />
                    <h3 class='media_title'>Tích điểm nhanh chóng</h3>
                    <p class='media_content m--0'>
                      Tích điểm đối với mỗi lượt đặt chỗ thành công. Quy đổi
                      thành Lux Credit để du lịch nhiều hơn nữa.
                    </p>
                  </div>
                </div>
                <div class='col-md-6 col-sm-6 col-xs-12'>
                  <div class='media'>
                    <img
                      class='img1'
                      src='https://www.luxstay.com/account/top-sales@2x.png'
                      width='55'
                      height='70'
                    />
                    <h3 class='media_title'>Tiện ích thông minh</h3>
                    <p class='media_content m--0'>
                      Check-in và kiểm tra hóa đơn thanh toán kể cả khi không có
                      kết nối mạng. Hoàn tiền nhanh gọn. Đổi lịch dễ dàng.
                    </p>
                  </div>
                </div>
                <div class='col-md-6 col-sm-6 col-xs-12'>
                  <div class='media'>
                    <img
                      class='img1'
                      src='	https://www.luxstay.com/account/wallet@2x.png'
                      width='60'
                      height='70'
                    />
                    <h3 class='media_title'>Thanh toán đơn giản</h3>
                    <p class='media_content m--0'>
                      Phương thức thanh toán tiện lợi, an toàn. Tích hợp chức
                      năng lưu thẻ để đặt phòng lần sau.
                    </p>
                  </div>
                </div>
                <div class='col-md-6 col-sm-6 col-xs-12'>
                  <div class='media'>
                    <img
                      class='img1'
                      src='https://www.luxstay.com/account/backpack@2x.png'
                      width='55'
                      height='70'
                    />
                    <h3 class='media_title'>Ưu đãi mỗi ngày</h3>
                    <p class='media_content m--0'>
                      Nhận thông báo ưu đãi từ {BRAND_NAME} khi có kế hoạch du lịch
                      để lựa chọn và đặt ngay cho mình một chỗ ở phù hợp, tiện
                      nghi với giá tốt nhất.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div class='col-md-4 col-xs-12 first-xs'>
              {isSignUp ? <SignUpForm host={host} handleSubmit={handleSubmit} /> : <LoginForm handleSubmit={handleLogin} />}
            </div>
          </div>
        </div>
      </div>
    </body>
  );
};

export default LoginPage;
