import { useFormik } from "formik";
import React from "react";
import { Link } from "react-router-dom";
import { BRAND_NAME } from "src/constants/commons";
import RoutesString from "src/routes/routesString";
import * as Yup from 'yup'
import "./SignUpForm.scss";
function SignUpForm({ host, handleSubmit }) {
  const { handleBlur, handleChange, handleSubmit: onSubmit, values, errors, touched } = useFormik({
    initialValues: {
      email: "",
      phoneNumber: "",
      firstName: "",
      lastName: "",
      password: "",
      confirmPassword: "",
    },
    validationSchema: Yup.object().shape({
      firstName: Yup.string().required("Required"),
      lastName: Yup.string().required("Required"),
      email: Yup.string().email("Invalid email").required("Required"),
      phoneNumber: Yup.string().required("Required").min(8, "Min 8").max(12, "Max 12"),
      password: Yup.string().required("Required").min(4, "Min 4"),
      confirmPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match')
    }),
    onSubmit: (values) => {
      handleSubmit(values);
    }
  })
  return (
    <div class="login">
      <form autocomplete="off" onSubmit={onSubmit}>
        <div class="signup_title">Đăng ký thành viên</div>
        <div>
          <label class="text_address">Địa chỉ email</label>
          <div class="input_group is-relative">
            <div class="input_group">
              <div class="input_group is-relative">
                <input name="email" type="text"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  class="input mt--0" aria-required="true" aria-invalid="true" />
                <span class="input_icon">
                  <i class="far fa-envelope"></i>
                </span>
              </div>
              <div className="text-red-500 italic">
                {errors.email && touched.email && <p>{errors.email}</p>}
              </div>
            </div>
            <div class="input_group">
              <div class="input_phonenumber">
                <label class="text_address">Số điện thoại</label>
                <div class="phone_input">
                  <input name="phoneNumber" type="text" placeholder="Số điện thoại"
                    onBlur={handleBlur}

                    value={values.phoneNumber}
                    onChange={handleChange}
                    aria-required="true" aria-invalid="true" />
                </div>
                <div className="text-red-500 italic">
                  {errors.phoneNumber && touched.phoneNumber && <p>{errors.phoneNumber}</p>}
                </div>
              </div>
            </div>
            <div class="input_group">
              <label class="text_address">Tên</label>
              <input name="firstName"
                onBlur={handleBlur}

                onChange={handleChange}
                value={values.firstName}
                type="text" class="input1" aria-required="true" aria-invalid="true" />
            </div>
            <div className="text-red-500 italic">
              {errors.firstName && touched.firstName && <p>{errors.firstName}</p>}
            </div>
            <div class="input_group">
              <label class="text_address">Họ và tên đệm</label>
              <input name="lastName"
                onBlur={handleBlur}

                value={values.lastName}
                onChange={handleChange}
                type="text" class="input1" aria-required="true" aria-invalid="true" />
            </div>
            <div className="text-red-500 italic">
              {errors.lastName && touched.lastName && <p>{errors.lastName}</p>}
            </div>
            <div class="mb--18">
              <label class="text_address1">
                <div class="bold">Mật khẩu</div>
                <div class="grey">(Tối thiểu 8 kí tự)</div>
              </label>
              <div class="input_group is-relative">
                <input name="password" value={values.password} onChange={handleChange} type="password" placeholder="Mật khẩu"
                  autocomplete="password" class="input" aria-required="true" aria-invalid="true" aria-autocomplete="list" />
                <span class="is-absolute input_icon">
                  <img
                    class="img2"
                    src="https://www.luxstay.com/icons/locked.svg"
                    width="14">
                  </img>
                </span>
              </div>
              <div className="text-red-500 italic">
                {errors.password && touched.password && <p>{errors.password}</p>}
              </div>
            </div>
            <div class="input_group">
              <label class="text_address">Xác nhận mật khẩu mới</label>
              <div class="input_group is-relative">
                <input name="confirmPassword" value={values.confirmPassword}
                  onBlur={handleBlur}
                  onChange={handleChange} data-vv-as="Xác nhận mật khẩu mới" type="password"
                  class="input" aria-required="false" aria-invalid="false" />
                <span class="is-absolute input_icon">
                  <img
                    class="img2"
                    src="https://www.luxstay.com/icons/locked.svg"
                    width="14">
                  </img>
                </span>
              </div>
            </div>
            <div className="text-red-500 italic">
              {errors.confirmPassword && touched.confirmPassword && <p>{errors.confirmPassword}</p>}
            </div>
          </div>
        </div>
        <div class="signup_body">
          <button type="submit" class="btn">Đăng ký </button>
          <div class="register">
            Bạn đã có tài khoản {BRAND_NAME}?
            <Link to={host ? RoutesString.HOST_LOGIN : RoutesString.LOGIN} class="colortext">
              Đăng nhập
            </Link>
          </div>
          <div class="caption center-xs">
            Tôi đồng ý với
            <a href="#" class="bold"> Bảo mật </a>
            và
            <a href="#" class="bold"> Điều khoản hoạt động </a>
            của {BRAND_NAME}.
          </div>
          {/* <div class="login_social">
            <div>
              <div class="btn_social">
                <a href="#" class="text3">
                  {" "}
                  Đăng nhập với Facebook
                </a>
                <div class="iconfb">
                  <i class="fab fa-facebook"></i>
                </div>
              </div>
            </div>
            <div>
              <div class="btn_social">
                <a href="#" class="text3">
                  {" "}
                  Đăng nhập với Google
                </a>
                <div class="icongg">
                  <i class="fab fa-google"></i>
                </div>
              </div>
            </div>
          </div> */}
        </div>

      </form>

    </div>
  );
}

export default SignUpForm;
