import React from 'react'
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import API from 'src/api';
import config from 'src/api/config';
import { METHOD } from 'src/constants/enums';
import RoutesString from 'src/routes/routesString';
import { getHomeById } from 'src/store/slices/clientSlice';
import { numberWithCommas } from 'src/utils/helper';
import './PaymentPage.scss'
function PaymentPage() {
    const dispatch = useDispatch();
    const { push } = useHistory();
    const { id } = useParams();
    const isLoading = useSelector(state => state.client.currentHomeLoading);
    const user = useSelector(state => state.auth.user);
    const home = useSelector(state => state.client.home);
    const { homeName,
        homeDescription,
        homeIntro,
        homeSize,
        homeImages,
        district,
        ward,
        street,
        apartmentNumber,
        homeRule,
        bathrooms,
        bedrooms,
        kitchens,
        province,
        homeType,
        owner,
        status,
        homeFees } = home
    useEffect(() => {
        dispatch(getHomeById(id))
    }, [])
    const dayOfWeek = new Date().getDay();
    const isWeekend = (dayOfWeek === 6) || (dayOfWeek === 0);
    const price = isWeekend ? homeFees?.[1]?.price : homeFees?.[0]?.price;
    const handlePayment = async () => {

        const data = {
            images: ["https://www.luxstay.com/logo@2x.png"],
            price,
            quantity: 1,
            name: homeName,
            cancelUrl: window.location.href
        }
        const response = await API({
            url: config.API.STRIPE_SESSION_SERVICE,
            method: METHOD.POST,
            data
        })
        window.location = response.session.url;
    }
    return (
        <div className="main">
            <div className="checkout">
                <div className="container container-sm">
                    <div className="row">
                        <div className="col-md-7 col-lg-6 col-xl-5 col-xs-12">
                            <div>
                                <div className="title mb--24">
                                    <div className="mb--6">Thanh toán</div>
                                    <p className="mb--0">Vui lòng chọn phương thức thanh toán</p>
                                </div>
                                {/* <div className="payment_card">
                                    <label className="el-radio ml-0 is-payment" role="radio" tabindex="0">
                                        <span className="el-radio_input">
                                            <span className="el-radio_inner"></span>
                                            <input type="radio" aria-hidden="true" tabindex="-1" className="el-radio_original" />
                                        </span>
                                        <span className="el-radio_label">
                                            <div className="row gatewway">
                                                <div className="col-xs-8 col-sm-10 gateway__desc">
                                                    <p className="gateway_title">Thẻ Visa, Thẻ Master, Thẻ JCB hoặc Thẻ American Express</p>
                                                    <span className="gateway_gray">Ghi nhận qua cổng thanh toán OnePay.</span>
                                                </div>
                                                <div className="col-xs-4 col-sm-2 text-right gateway__icon">
                                                    <img src="https://cdn.luxstay.com/images/logos/payments/visa_master_jcb.svg" alt="" />
                                                </div>
                                            </div>
                                        </span>
                                    </label>
                                </div> */}
                                <div className="payment_card">
                                    <label className="el-radio ml-0 is-payment" role="radio" tabindex="0">
                                        <span className="el-radio_input">
                                            <span className="el-radio_inner"></span>
                                            <input type="radio" aria-hidden="true" tabindex="-1" className="el-radio_original" />
                                        </span>
                                        <span className="el-radio_label">
                                            <div className="row gatewway">
                                                <div className="col-xs-8 col-sm-10 gateway__desc">
                                                    <p className="gateway_title">Thẻ Visa, Thẻ Master, thẻ JCB</p>
                                                    <span className="gateway_gray">Nhập thông tin thẻ hoặc lựa chọn thẻ đã lưu để tiến hành thanh toán qua cổng Stripe.</span>
                                                </div>
                                                <div className="col-xs-4 col-sm-2 text-right gateway__icon">
                                                    <img src="https://cdn.luxstay.com/images/logos/payments/visa_master_jcb.svg" alt="" />
                                                </div>
                                            </div>
                                        </span>
                                    </label>
                                </div>
                                <button className="btn_payment" onClick={handlePayment}>Thanh toán ngay</button>
                            </div>
                        </div>
                        <div className="col-md-1 col-lg-2 col-xl-3 col-xs-12"></div>
                        <div className="col-md-4 d-none d-md-block">
                            <div className="h--100">
                                <div className="title">
                                    <div className="details_payment">Chi tiết đặt phòng</div>
                                </div>
                                <div className="checkup">
                                    <div className="checkup_header">
                                        <a href="#" className="is-flex">
                                            <div className="grow">
                                                <div className="checkup_title">{homeName}</div>
                                                <div className="checkup_add">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    <span className="medium" >{province}, {district}, {ward}</span>
                                                </div>
                                            </div>
                                            <div className="checkup_right">
                                                <img src={homeImages?.[0]} alt="" />
                                            </div>
                                        </a>
                                    </div>
                                    <div className="checkup_body">
                                        <div className="checkup_detail">
                                            <div className="is-flex middle-xs">
                                                <i class="far fa-calendar-alt"></i>
                                                <span>
                                                    <b>1 đêm</b>
                                                    : 30/12/2021 - 31/12/2021
                                                </span>
                                            </div>
                                            <div className="is-flex middle-xs">
                                                <i class="fas fa-user-circle"></i>
                                                <span>
                                                    <b>1 người lớn</b>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="checkup_price">
                                            <div className="is-flex middle-xs between-xs">
                                                <div className="is-flex price_left">
                                                    <span className="pr--6">Giá thuê 1 đêm</span>
                                                </div>
                                                <span className="price_right">{numberWithCommas(price)}₫</span>
                                            </div>

                                            <hr className="my--18" />
                                            <div className="is-flex middle-xs between-xs">
                                                <div className="is-flex price_left">
                                                    <span className="pr--6 extra-bold">Tổng tiền</span>
                                                </div>
                                                <span className="price_right extra-bold">{numberWithCommas(price)}₫</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="checkup_footer">
                                        <div className="cancel_group">
                                            <div className="cancel_title">Chính sách hủy phòng</div>
                                            <span className="cancel_text">
                                                <b>Trung bình</b> : Miễn phí hủy phòng trong vòng 48h sau khi đặt phòng thành công
                                                và trước 5 ngày so với thời gian check-in. Sau đó, hủy phòng trước 5 ngày so với
                                                thời gian check-in, được hoàn lại 100% tổng số tiền đã trả (trừ phí dịch vụ).
                                            </span>
                                            <a href="#" className="color-i">Chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PaymentPage
