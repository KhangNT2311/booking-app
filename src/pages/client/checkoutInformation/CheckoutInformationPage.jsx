import React from 'react'
import { useEffect } from 'react';
import CircularProgress from '@mui/material/CircularProgress';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { getHomeById } from 'src/store/slices/clientSlice';
import './CheckoutInformationPage.scss'
import { numberWithCommas } from 'src/utils/helper';
import RoutesString from 'src/routes/routesString';
function CheckoutInformationPage() {
    const dispatch = useDispatch();
    const { push } = useHistory();
    const { id } = useParams();
    const isLoading = useSelector(state => state.client.currentHomeLoading);
    const user = useSelector(state => state.auth.user);
    const home = useSelector(state => state.client.home);
    const { homeName,
        homeDescription,
        homeIntro,
        homeSize,
        homeImages,
        district,
        ward,
        street,
        apartmentNumber,
        homeRule,
        bathrooms,
        bedrooms,
        kitchens,
        province,
        homeType,
        owner,
        status,
        homeFees } = home
    useEffect(() => {
        dispatch(getHomeById(id))
    }, [])
    const dayOfWeek = new Date().getDay();
    const isWeekend = (dayOfWeek === 6) || (dayOfWeek === 0);
    const price = isWeekend ? homeFees?.[1]?.price : homeFees?.[0]?.price;
    const handlePayment = () => {
        push(RoutesString.CHECKOUT_PAYMENT.replace(":id", id))
    }
    return (
        <div className="main">
            {isLoading ? <div className='d-flex justify-center h-screen items-center'>
                <CircularProgress />
            </div> : <div className="checkout">
                <div className="container container-sm">
                    <div className="row">
                        <div className="col-md-6 col-xs-12">
                            <div>
                                <div className="title">
                                    <h3>Thông tin đặt chỗ</h3>
                                </div>
                                <div className="space_lose">
                                    <div className="input_group">
                                        <label className="name_checkout">
                                            <span className="text_danger">* </span>
                                            Số khách
                                        </label>
                                        <div className="row mt1">
                                            <div className="col-xs-6">
                                                <div className="box_number"> 1 khách </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="input_group">
                                        <label className="name_checkout">
                                            1 đêm tại {homeName}
                                        </label>
                                        <div className="row mt1">
                                            <div className="col-xs-6">
                                                <div className="check_inout">
                                                    <hr className="green_checkin"></hr>
                                                    <p className="checkin_title">Nhận phòng</p>
                                                    <p className="checkin_title1">30/12/2021</p>
                                                    <p className="checkin_title2">Thứ năm</p>
                                                </div>
                                            </div>
                                            <div className="col-xs-6">
                                                <div className="check_inout">
                                                    <hr className="orange_checkout"></hr>
                                                    <p className="checkin_title">Trả phòng</p>
                                                    <p className="checkin_title1">31/12/2021</p>
                                                    <p className="checkin_title2">Thứ sáu</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="input_group">
                                        <label className="name_checkout">Trách nhiệm vật chất</label>
                                        <span className="input-group__text is-block">
                                            Khách hàng chịu mọi trách nhiệm thiệt hại về tài sản đã gây ra tại
                                            chỗ ở trong thời gian lưu trú.
                                        </span>
                                    </div>
                                    <div className="input_group">
                                        <label className="name_checkout">Nội quy chỗ ở</label>
                                        <span className="input-group__text is-block">Yêu cầu chứng minh thư/ căn cước
                                            công dân/ hộ chiếu hoặc đặt cọc tại chỗ nghỉ\nHạn chế làm ồn sau 10 giờ tối\n
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div className="title mt--48">
                                <h3>Thông tin của bạn</h3>
                            </div>
                            <div className="space_lose">
                                <div className="input_group">
                                    <label className="name_checkout">
                                        <span className="text_danger">* </span>
                                        Tên khách hàng
                                    </label>
                                    <span className="input-group__text is-block">Họ tên trên CMND/ Thẻ căn cước</span>
                                    <input name="name" value={user?.name} className="input_info" aria-required="true" aria-invalid="false" />
                                </div>
                                <div className="row input_group">
                                    <div className="col-xs-12 col-sm-6">
                                        <div className="input_group">
                                            <label className="name_checkout">
                                                <span className="text_danger">* </span>
                                                Số điện thoại
                                            </label>
                                            <span className="input-group__text is-block">Mã điện thoại quốc gia</span>
                                            <input name="customer_phone" value={user?.phoneNumber} className="input_info" placeholder="Số điện thoại" aria-required="true" aria-invalid="false" />
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-sm-6">
                                        <div className="input_group">
                                            <label className="name_checkout">
                                                <span className="text_danger">* </span>
                                                Email
                                            </label>
                                            <span className="input-group__text is-block">VD: user1@gmail.com</span>
                                            <input type="email" value={user?.email} className="input_info" aria-required="true" aria-invalid="false" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row input_group">
                                    <div className="col-xs-12">
                                        <label className="name_checkout">
                                            <span className="text_danger">* </span>
                                            Quốc gia cư trú
                                        </label>
                                        <span className="input-group__text is-block">Nội dung này sẽ được sử dụng cho vấn đề pháp lý và thuế.</span>
                                    </div>
                                    <div className="col-xs-12 col-sm-6">
                                        <div className="mt">
                                            <div className="box_number"> Việt Nam </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="title mt--48">
                                    <h3>Mã khuyến mại</h3>
                                </div>
                                <form className="input_group">
                                    <input type="text" className="input_info" placeholder="Nhập mã khuyến mại" aria-required="true" aria-invalid="false" />
                                    <button className="btn btn_apply">Áp dụng</button>
                                </form>
                                <button className="checkout_btn" onClick={handlePayment}>Thanh toán</button>
                            </div>
                        </div>
                        <div className="col-lg-2 col-md-1"></div>
                        <div className="col-md-5 col-lg-4 col-xs-12 d-none d-md-block">
                            <div className="h--100">
                                <div className="title">
                                    <div className="details_payment">Chi tiết đặt phòng</div>
                                </div>
                                <div className="checkup">
                                    <div className="checkup_header">
                                        <a href="#" className="is-flex">
                                            <div className="grow">
                                                <div className="checkup_title">{homeName}</div>
                                                <div className="checkup_add">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    <span className="medium" >{province}, {district}, {ward}</span>
                                                </div>
                                            </div>
                                            <div className="checkup_right">
                                                <img src={homeImages?.[0]} alt="" />
                                            </div>
                                        </a>
                                    </div>
                                    <div className="checkup_body">
                                        <div className="checkup_detail">
                                            <div className="is-flex middle-xs">
                                                <i class="far fa-calendar-alt"></i>
                                                <span>
                                                    <b>1 đêm</b>
                                                    : 30/12/2021 - 31/12/2021
                                                </span>
                                            </div>
                                            <div className="is-flex middle-xs">
                                                <i class="fas fa-user-circle"></i>
                                                <span>
                                                    <b>1 người lớn</b>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="checkup_price">
                                            <div className="is-flex middle-xs between-xs">
                                                <div className="is-flex price_left">
                                                    <span className="pr--6">Giá thuê 1 đêm</span>
                                                </div>
                                                <span className="price_right">{numberWithCommas(price)}<sup>đ</sup></span>
                                            </div>

                                            <hr className="my--18" />
                                            <div className="is-flex middle-xs between-xs">
                                                <div className="is-flex price_left">
                                                    <span className="pr--6 extra-bold">Tổng tiền</span>
                                                </div>
                                                <span className="price_right extra-bold">{numberWithCommas(price)}<sup>₫</sup> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="checkup_footer">
                                        <div className="cancel_group">
                                            <div className="cancel_title">Chính sách hủy phòng</div>
                                            <span className="cancel_text">
                                                <b>Trung bình</b> : Miễn phí hủy phòng trong vòng 48h sau khi đặt phòng thành công
                                                và trước 5 ngày so với thời gian check-in. Sau đó, hủy phòng trước 5 ngày so với
                                                thời gian check-in, được hoàn lại 100% tổng số tiền đã trả (trừ phí dịch vụ).
                                            </span>
                                            <a href="#" className="color-i">Chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>}

        </div>
    )
}

export default CheckoutInformationPage
