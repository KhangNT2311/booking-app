import { Icon } from '@iconify/react';
import pieChart2Fill from '@iconify/icons-eva/pie-chart-2-fill';
import peopleFill from '@iconify/icons-eva/people-fill';
import shoppingBagFill from '@iconify/icons-eva/shopping-bag-fill';
import fileTextFill from '@iconify/icons-eva/file-text-fill';
import lockFill from '@iconify/icons-eva/lock-fill';
import personAddFill from '@iconify/icons-eva/person-add-fill';
import alertTriangleFill from '@iconify/icons-eva/alert-triangle-fill';
import RoutesString from 'src/routes/routesString';

// ----------------------------------------------------------------------

const getIcon = (name) => <Icon icon={name} width={22} height={22} />;

const sidebarConfig = [
  {
    title: 'dashboard',
    path: RoutesString.DASHBOARD,
    icon: getIcon(pieChart2Fill)
  },
  {
    title: 'Chỗ Nghỉ',
    path: RoutesString.ADMIN_HOMES,
    icon: getIcon(peopleFill)
  },
  {
    title: 'Loại Chỗ Nghỉ',
    path: RoutesString.ADMIN_HOME_TYPES,
    icon: getIcon(peopleFill)
  },
];

export default sidebarConfig;
