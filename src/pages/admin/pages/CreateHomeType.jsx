import { Link as RouterLink, useHistory, useParams } from 'react-router-dom';
// material
import { styled } from '@mui/material/styles';
import { Box, Card, Link, Container, Typography, CircularProgress } from '@mui/material';
// layouts
import AuthLayout from '../layouts/AuthLayout';
// components
import Page from '../components/Page';
import { MHidden } from '../components/@material-extend';
import { RegisterForm } from '../components/authentication/register';
import AuthSocial from '../components/authentication/AuthSocial';
import CreateHomeTypeForm from './CreateHomeTypeForm';
import { useDispatch } from 'react-redux';
import { useState } from 'react';
import API from 'src/api';
import config from 'src/api/config';
import { METHOD } from 'src/constants/enums';
import { useEffect } from 'react';
import { TiArrowLeft } from 'react-icons/ti';
import RoutesString from 'src/routes/routesString';

// ----------------------------------------------------------------------

const RootStyle = styled(Page)(({ theme }) => ({
    [theme.breakpoints.up('md')]: {
        display: 'flex'
    }
}));

const SectionStyle = styled(Card)(({ theme }) => ({
    width: '100%',
    maxWidth: 464,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    margin: theme.spacing(2, 0, 2, 2)
}));

const ContentStyle = styled('div')(({ theme }) => ({
    maxWidth: 480,
    margin: 'auto',
    display: 'flex',
    minHeight: '70vh',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: theme.spacing(12, 0)
}));

// ----------------------------------------------------------------------

export default function CreateHomeTypePage() {
    const { id } = useParams();
    const { push } = useHistory();
    const dispatch = useDispatch();
    const [homeType, setHomeType] = useState();
    const [isLoading, setIsLoading] = useState(true);
    useEffect(() => {
        if (id) {
            (async () => {
                setIsLoading(true);
                const response = await API({
                    url: `${config.API.HOME_TYPE_SERVICE}/${id}`,
                    method: METHOD.GET
                })
                setIsLoading(false);
                setHomeType(response)
            })()
        } else {
            setIsLoading(false)
        }
    }, [])
    const handleGoBack = () => {
        push(RoutesString.ADMIN_HOME_TYPES)
    }
    return (
        <RootStyle title="CreateHomeType | Minimal-UI">
            <TiArrowLeft className='pl-5 w-20 h-20 cursor-pointer' onClick={handleGoBack} />
            <Container>
                <ContentStyle>
                    {isLoading ?
                        <>

                            <div className='d-flex justify-center  items-center'>
                                <CircularProgress />
                            </div>
                        </> :
                        <>
                            <Box sx={{ mb: 5 }}>
                                <Typography variant="h4" gutterBottom>
                                    {id ? "Chỉnh sửa" : "Tạo"} loại chỗ nghỉ
                                </Typography>
                            </Box>
                            <CreateHomeTypeForm isUpdate={!!id} homeType={homeType} />
                        </>
                    }
                </ContentStyle>
            </Container>
        </RootStyle>
    );
}
