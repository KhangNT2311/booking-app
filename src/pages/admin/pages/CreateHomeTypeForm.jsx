import * as Yup from 'yup';
import { useState } from 'react';
import { Icon } from '@iconify/react';
import { useFormik, Form, FormikProvider } from 'formik';
import eyeFill from '@iconify/icons-eva/eye-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import { useHistory } from 'react-router-dom';
// material
import { Stack, TextField, IconButton, InputAdornment, textAre } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import RoutesString from 'src/routes/routesString';
import API from 'src/api';
import config from 'src/api/config';
import { METHOD } from 'src/constants/enums';
import { toast } from 'react-toastify';
import { useDispatch } from 'react-redux';
import { createHomeType, updateHomeType } from 'src/store/slices/adminSlice';

// ----------------------------------------------------------------------

export default function CreateHomeTypeForm({ isUpdate, homeType }) {
  const { push } = useHistory();
  const dispatch = useDispatch()
  const RegisterSchema = Yup.object().shape({
    homeTypeName: Yup.string()
      .min(2, 'Too Short!')
      .max(50, 'Too Long!')
      .required('First name required'),
    description: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Last name required'),
  });

  const formik = useFormik({
    initialValues: {
      homeTypeName: homeType?.homeTypeName || '',
      description: homeType?.description || ''
    },
    enableReinitialize: true,
    validationSchema: RegisterSchema,
    onSubmit: async (values) => {

      if (isUpdate) {
        dispatch(updateHomeType(homeType?._id, values, push))
      } else {
        dispatch(createHomeType(values, push))
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <Stack spacing={3}>
          <TextField
            fullWidth
            type="text"
            label="Tên loại chỗ nghỉ"
            {...getFieldProps('homeTypeName')}
            error={Boolean(touched.email && errors.email)}
            helperText={touched.email && errors.email}
          />
          <TextField
            fullWidth
            type="email"
            label="Mô tả"
            {...getFieldProps('description')}
            error={Boolean(touched.email && errors.email)}
            helperText={touched.email && errors.email}
          />

          <LoadingButton
            fullWidth
            size="large"
            type="submit"
            variant="contained"
            loading={isSubmitting}
          >
            {isUpdate?"Chỉnh sửa":"Tạo"}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
