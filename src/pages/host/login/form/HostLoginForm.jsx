import React from 'react'
import { TiSocialFacebook } from 'react-icons/ti'
import { AiOutlineGooglePlus } from 'react-icons/ai'
import { useFormik } from 'formik'
import InputField from 'src/components/atoms/input/InputField'
function HostLoginForm({handleLogin}) {
    const { handleSubmit, handleChange, values } = useFormik({
        initialValues: {
            email: "",
            password: ""
        },
        onSubmit: (values) => {
            handleLogin(values);
        }
    })
    return (
        <div className='row-auto border-gray-300 border-r p-4'>
            <button class="bg-fb hover:opacity-75  w-full text-white   hover:bg-grey text-grey-darkest font-bold py-2 px-4 rounded inline-flex items-center justify-center">
                <TiSocialFacebook className='w-6 h-6 mr-1' />
                <span>Đăng nhập với Facebook</span>
            </button>
            <button class="bg-gg hover:opacity-75 w-full mt-3 text-center text-white hover:bg-grey text-grey-darkest font-bold py-2 px-4 rounded inline-flex items-center justify-center">
                <AiOutlineGooglePlus className='w-6 h-6 mr-1' />
                <span>Đăng nhập với Facebook</span>
            </button>
            <div className='or-line bg-gray-300 w-full mt-4 h-px text-gray-500'></div>
            <form className='mt-4' onSubmit={handleSubmit}>
                <div>
                    <InputField
                        label="Email" 
                        placeholder="Email"
                        id={`emailId`}
                        name={"email"}
                        value={values.email}
                        onChange={handleChange}
                        type={"email"}
                    />
                </div>
                <div className='mt-4'>
                    <InputField 
                    label="Mật khẩu"
                     placeholder="Nhập mật khẩu"
                     id={`passwordId`}
                     name={"password"}
                     value={values.password}
                     onChange={handleChange}
                     type={"password"} />
                </div>

                <div className="flex items-center justify-between my-4">
                    <div className="flex items-center">
                        <input
                            id="remember-me"
                            name="remember-me"
                            type="checkbox"
                            className="h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded"
                        />
                        <label htmlFor="remember-me" className="ml-2 block text-sm text-gray-900">
                            Remember me
                        </label>
                    </div>

                    <div className="text-sm">
                        <a href="#" className="font-medium text-indigo-600 hover:text-indigo-500">
                            Forgot your password?
                        </a>
                    </div>
                </div>

                <div>
                    <button
                        type="submit"
                        className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    >
                        {/* <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                            <LockClosedIcon className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" aria-hidden="true" />
                        </span> */}
                        Sign in
                    </button>
                </div>
            </form>


        </div>
    )
}

export default HostLoginForm
