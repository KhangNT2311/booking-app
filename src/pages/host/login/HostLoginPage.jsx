import React from 'react'

import { Link } from 'react-router-dom'
import './HostLoginPage.scss'
import HostLoginForm from './form/HostLoginForm'
import RoutesString from 'src/routes/routesString'
import { BRAND_NAME } from 'src/constants/commons'
import { login } from 'src/store/slices/authSlice';
import { USER_ROLE } from 'src/constants/enums';
import { useDispatch } from 'react-redux';

function HostLoginPage() {
    const dispatch = useDispatch();
    const handleLogin = async (values) => {
        login(values, USER_ROLE.HOST)(dispatch)
    }
    return (
        <div className=''>
            <div className='container'>
                <div className='grid place-items-center '>
                    <div className='grid grid-cols-2 w-4/5 border-gray-300 border col-span-10'>
                        <HostLoginForm handleLogin={handleLogin} />
                        <div className='p-4'>
                            <p >Chưa đăng ký? <Link className='text-primary' to={RoutesString.SIGN_UP + "&host=true"}>Nhấn vào đây để đăng ký</Link></p>
                            <p>Chúng tôi không thu phí khi bạn đăng chỗ nghỉ. Nếu chỗ nghỉ của bạn đạt tiêu chuẩn được kiểm duyệt đăng tải trên {BRAND_NAME}, chúng tôi chỉ thu phí khi có booking
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HostLoginPage
