import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom';
import { BRAND_NAME } from 'src/constants/commons';
import RoutesString from 'src/routes/routesString';
import banner from '../../../assets/images/host/banner_host.svg'
function GetStartedPage() {
    const user = useSelector(state => state.auth?.user);
    return (
        <div className=' d-flex items-center justify-center h-screen'>
            <div className='container'>
                <div className='grid grid-cols-2'>
                    <div >
                        <h1 className='text-3xl font-bold mb-3'>
                            Xin chào, {user?.name}
                        </h1>
                        <p>Bạn đã sẵn sàng chia sẻ chỗ nghỉ tuyệt đẹp của mình với {BRAND_NAME}?</p>
                        <div className='mt-4'>
                        <Link to={RoutesString.HOST_ACCOUNT_SETTINGS} class="mt-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                            Tạo chỗ nghỉ mới
                        </Link>
                        </div>
                    </div>
                    <div>
                        <img src={banner} alt="banner" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default GetStartedPage
