import React from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom';
import { STATUS } from 'src/constants/enums';
import RoutesString from 'src/routes/routesString';
import { getListHostHomes } from 'src/store/slices/hostSlice';
import { numberWithCommas } from 'src/utils/helper';

function HostHomeManagement() {
    const dispatch = useDispatch();
    const { push } = useHistory();
    const homes = useSelector(state => state.host.homes);
    useEffect(() => {
        dispatch(getListHostHomes)
    }, [])
    const titles = ["STT", "TÊN CHỖ NGHỈ", "ĐỊA ĐIỂM", "GIÁ PHÒNG", "TRẠNG THÁI", "HÀNH ĐỘNG"]
    const renderStatus = (status) => {
        switch (status) {
            case STATUS.VOID:
                return <div className="text-left font-medium text-red-500">ĐÃ TỪ CHỐI</div>

            case STATUS.PENDING:
                return <div className="text-left font-medium text-yellow-500">ĐANG XÉT DUYỆT</div>

            default:
                return <div className="text-left font-medium text-green-500">ĐÃ DUYỆT</div>
        }
    }
    const handleUpdateHome = (id) => () => {
        push(RoutesString.INFOMATION_STEP_UPDATE.replace(":id", id))
    }
    const renderRows = (homes) => homes.map((home, idx) => {
        return <tr key={`home-${idx}`}>
            <td className="p-2 whitespace-nowrap">
                <div className="flex items-center">
                    <p>{idx + 1}</p>
                </div>
            </td>
            <td className="p-2 whitespace-nowrap">
                <div className="flex items-center">
                    <div class="w-20 h-16 flex-shrink-0 mr-2 sm:mr-3"><img class="rounded  w-full h-full" src={home?.homeImages?.[0]} alt={home?.homeName} /></div>
                    <div className="font-medium text-gray-800">{home?.homeName}</div>
                </div>
            </td>
            <td className="p-2 whitespace-nowrap">
                <div className="text-left">{`${home?.apartmentNumber}, ${home?.street}, ${home?.ward}, ${home?.district}, ${home?.province}`} </div>
            </td>
            <td className="p-2 whitespace-nowrap">
                <div className=" text-left">
                    <p>Thứ 2 - Thứ 6: {numberWithCommas(home?.homeFees?.[0]?.price)} <sup>đ</sup></p>
                    <p>Thứ 7 - Chủ nhật: {numberWithCommas(home?.homeFees?.[1]?.price)} <sup>đ</sup></p>
                </div>
            </td>
            <td className="p-2 whitespace-nowrap">
                {renderStatus(home?.status)}
            </td>
            <td className="p-2 whitespace-nowrap">
                <div className="text-center">
                    <button onClick={handleUpdateHome(home?._id)} class="p-2 pl-5 pr-5 bg-transparent border-2 border-yellow-500 text-yellow-500  rounded-lg hover:bg-yellow-500 hover:text-gray-100 focus:border-4 focus:border-yellow-300">Chỉnh sửa</button>
                </div>
            </td>
        </tr>
    })
    return (
        <div className='hostHomeManagement'>
            <div className='pt-10'>
                <h2 className='text-3xl font-bold'>
                    {homes.length} Chỗ nghỉ
                </h2>
                <div>
                    <section className="text-gray-600 h-screen ">
                        <div className="mt-10">
                            <div className="w-full mx-auto bg-white shadow-lg rounded-sm border border-gray-200">
                                <div className="p-3">
                                    <div className="overflow-x-auto">
                                        <table className="table-auto w-full">
                                            <thead className="text-xs font-semibold uppercase text-gray-400 bg-gray-50">
                                                <tr>
                                                    {titles.map((title, idx) => <th key={`${title}-${idx}`} className="p-2 whitespace-nowrap">
                                                        <div className="font-semibold text-left">{title}</div>

                                                    </th>)}
                                                </tr>
                                            </thead>
                                            <tbody className="text-sm divide-y divide-gray-100">
                                                {renderRows(homes)}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    )
}

export default HostHomeManagement
