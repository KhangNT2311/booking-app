import { stateToHTML } from 'draft-js-export-html'
import { useFormik } from 'formik'
import React from 'react'
import { useState } from 'react'
import { useMemo } from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CustomEditor from 'src/components/atoms/customEditor/CustomEditor'
import InputField from 'src/components/atoms/input/InputField'
import { BRAND_NAME } from 'src/constants/commons'
import { getHomeTypes } from 'src/store/slices/homeSlice'
import local from '../../../../mockData/local';
import Dropzone from 'src/components/molecules/dropzone/Dropzone';

import './InformationStep.scss'
import { METHOD, RULE } from 'src/constants/enums'
import { uploadMultipleFiles } from 'src/services/commons'
import API from 'src/api'
import config from 'src/api/config'
import { toast } from 'react-toastify';

function InformationStep() {
    const homeTypes = useSelector(state => state?.home?.homeTypes);
    const dispatch = useDispatch();
    const [previewImages, setPreviewImages] = useState([]);
    const { setFieldValue, errors, values, handleChange, handleBlur, handleSubmit } = useFormik({
        initialValues: {
            homeType: "",
            homeName: "",
            province: "",
            district: "",
            ward: "",
            street: "",
            apartmentNumber: "",
            homeSize: 0,
            bedrooms: 0,
            bathrooms: 0,
            kitchens: 0,
            homeRule: "",
            homeDescription: "",
            homeFeeWeekday: 0,
            homeFeeWeekend: 0,
            newImages: []
        },
        onSubmit: async (values) => {
            const id = toast.loading("Loading...")

            const { newImages, homeFeeWeekend, homeFeeWeekday, ...updatedValues } = values;
            updatedValues.homeFees = [{
                rule: RULE.WEEKDAY,
                price: homeFeeWeekday
            }, {
                rule: RULE.WEEKEND,
                price: homeFeeWeekend
            }];
            updatedValues.homeIntro = `${updatedValues.homeName} · ${updatedValues.bathrooms} Phòng tắm · ${updatedValues.bedrooms} Phòng ngủ · ${updatedValues.kitchens} Phòng bếp`
            try {
                const imageUrls = await uploadMultipleFiles(newImages);
                updatedValues.homeImages = imageUrls;
                const response = await API({
                    url: config.API.HOME_SERVICE,
                    method: METHOD.POST,
                    data: updatedValues,
                    hasToken: true
                })
                console.log("response", response);
                toast.update(id, {
                    render: "Tạo phòng thành công!", type: "success", isLoading: false, autoClose: 3000
                });
            } catch (error) {
                toast.update(id, {
                    render: error.response.data.message || "Oops... Có lỗi đang xảy ra", type: "error", isLoading: false, autoClose: 3000
                });
            }
        }
    });

    useEffect(() => {
        setFieldValue("district", "");
        setFieldValue("ward", "");
    }, [values.province])
    useEffect(() => {
        setFieldValue("ward", "");
    }, [values.district])

    const currentProvince = useMemo(() => {
        if (values.province) {
            return local.find((item) => item.name === values.province)
        }
        return null;
    }, [values.province])

    const currentDistrict = useMemo(() => {
        if (currentProvince) {
            return currentProvince.districts.find((item) => item.name === values.district)
        }
        return null;
    }, [values.district, currentProvince])

    const currentWard = useMemo(() => {
        if (currentProvince) {
            return currentProvince.districts.find((item) => item.name === values.district)
        }
        return null;
    }, [values.wrad, currentDistrict])

    useEffect(() => {
        dispatch(getHomeTypes)
    }, [])
    const options = homeTypes.map((item) => ({
        label: item.homeTypeName,
        value: item._id
    }))
    const hint = homeTypes.find(item => item._id === values.homeType);
    const cities = local.map((item) => ({
        label: item.name,
        value: item.name
    }));

    const districts = currentProvince && currentProvince?.districts?.map((item) => ({ value: item.name, label: item.name }))
    const wards = currentDistrict && currentDistrict?.wards?.map((item) => ({ value: item.name, label: item.name }))
    const onEditorChange = (name) => (e) => {
        const value = stateToHTML(e.getCurrentContent())
        setFieldValue(name, value)
    }
    const handleUpload = (file) => {
        if (file) {
            setPreviewImages([...previewImages, URL.createObjectURL(file)]);
            setFieldValue("newImages", [...values.newImages, file])
        }
    }
    return (
        <form className='container' onSubmit={handleSubmit}>
            <div className='row'>
                <div className='col-8'>
                    <div className='bg-white mb-40 border-gray-300 border relative'>
                        <div className='p-4 border-gray-300 border-b'>
                            <h2 className='font-bold text-xl pb-2'>Phân loại chỗ nghỉ</h2>
                            <p className='text-sm text-gray-600'>Trước hết, hãy cho chúng tôi biết, chỗ nghỉ của bạn thuộc loại hình nào</p>
                        </div>
                        <div className='p-4 '>
                            <InputField
                                name="homeType"
                                value={values.homeType}
                                type="select"
                                options={[{ label: "Chọn loại chỗ ở", value: null }, ...options]}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                label="CHỖ NGHỈ CỦA BẠN LÀ:"
                                required={true}
                            />
                            <div className='hint absolute top-0 left-full p-4 translate-x-4 transform w-72'>
                                <p className='font-bold text-sm pb-2'>
                                    Tại sao cần phân loại chỗ nghỉ?
                                </p>
                                <p className='text-xs'>
                                    Tại {BRAND_NAME}, chúng tôi phân chỗ nghỉ thành 29 loại, việc này giúp cho khách hàng lựa chọn nơi ở dễ dàng hơn. Đồng thời Luxstay cũng có điều kiện hỗ trợ bạn tốt hơn.
                                </p>
                            </div>
                        </div>
                        {hint && <div className='hint mx-4'>
                            <div className='p-4'>
                                <p className='text-xl pb-2 '>{hint?.homeTypeName}</p>
                                <p className=' text-xs'>{hint?.description}</p>
                            </div>
                        </div>}
                        <div className='p-4 relative'>
                            <InputField
                                required={true}
                                name="homeName"
                                value={values.homeName}
                                type="text"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                label="TÊN CHỖ NGHỈ:"
                            />
                            <div className='hint absolute top-0 left-full p-4 translate-x-4 transform w-72'>
                                <p className='font-bold text-sm pb-2'>
                                    Gợi ý đặt tên chỗ nghỉ để thu hút khách hàng và tối ưu tìm kiếm trên Google, {BRAND_NAME}

                                </p>
                                <p className='text-xs'>
                                    <p className=''>
                                        - Một cái tên thu hút thường bao gồm: Tên nhà + Tên phòng + Đặc điểm nổi bật tại chỗ nghỉ + Địa điểm du lịch <br />
                                        - Ví dụ tên chỗ nghỉ tốt: LuxHome - Phòng hoa hồng có máy chiếu, 5p tới hồ Gươm <br />
                                        - Không nên đặt tên: Phòng hoa hồng tại LuxHome
                                    </p>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className='bg-white mb-20 border-gray-300 border relative'>
                        <div className='p-4 border-gray-300 border-b'>
                            <h2 className='font-bold text-xl pb-2'>Vị trí chỗ nghỉ</h2>
                        </div>
                        <div className='px-4 pt-4 pb-2 '>
                            <InputField
                                name="province"
                                value={values.province}
                                type="select"
                                options={[{ label: "Chọn tỉnh/thành phố", value: "" }, ...cities]}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                label="TỈNH/THÀNH PHỐ: "
                                required={true}
                            />
                            <div className='hint absolute top-0 left-full p-4 translate-x-4 transform w-72'>
                                <p className='font-bold text-sm pb-2'>
                                    Ghi địa chỉ như thế nào là đúng?
                                </p>
                                <p className='text-xs'>
                                    Địa chỉ Homestay của bạn sẽ được cung cấp cho Khách hàng khi họ hoàn tất đặt chỗ. Đối với địa chỉ nhà cụ thể (ngõ/ ngách), bạn ghi theo thứ tự lần lượt là: Ngõ/kiệt/ngách/hẻm.

                                    Ví dụ: Số nhà 250 ngách 78 ngõ 162 Kim Mã, Ba Đình, Hà Nội sẽ được điền là: 162/78/250 Kim Mã, Ba Đình, Hà Nội hoặc SN 250 ngõ 162/78 Ba Đình, Hà Nội.

                                    Sau khi điền đầy đủ và chính xác những thông tin chúng tôi yêu cầu, vui lòng kiểm tra vị trí Homestay của bạn hiển thị trên bản đồ.                                </p>
                            </div>
                        </div>
                        <div className='px-4 py-2 relative'>
                            <InputField
                                required={true}
                                name="district"
                                value={values.district}
                                type="select"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                label="QUẬN/ HUYỆN:"
                                options={districts}
                                readonly={!!!values.province}
                            />
                        </div>

                        <div className='px-4 py-2'>
                            <InputField
                                required={true}
                                name="ward"
                                value={values.ward}
                                type="select"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                label="PHƯỜNG/ XÃ:"
                                options={wards}
                                readonly={!!!values.district}
                            />
                        </div>
                        <div className='px-4 py-2'>
                            <InputField
                                required={true}
                                name="street"
                                value={values.street}
                                type="text"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                label="ĐƯỜNG:"
                            />
                        </div>
                        <div className='px-4 py-2'>
                            <InputField
                                required={true}
                                name="apartmentNumber"
                                value={values.apartmentNumber}
                                type="text"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                label="SỐ NHÀ:"
                            />
                        </div>
                    </div>
                    <div className='bg-white mb-20 border-gray-300 border relative'>
                        <div className='p-4 border-gray-300 border-b'>
                            <h2 className='font-bold text-xl pb-2'>Không gian</h2>
                        </div>
                        <div className='px-4 pt-4 pb-2 '>
                            <InputField
                                name="homeSize"
                                value={values.homeSize}
                                type="number"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                label="CHỖ NGHỈ CỦA BẠN CÓ DIỆN TÍCH LÀ? M2:"
                                required={true}
                            />

                        </div>
                        <div className='px-4 py-2 relative'>
                            <InputField
                                required={true}
                                name="bedrooms"
                                value={values.bedrooms}
                                type="number"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                label="SỐ PHÒNG NGỦ: "
                            />
                        </div>

                        <div className='px-4 py-2'>
                            <InputField
                                required={true}
                                name="bathrooms"
                                value={values.bathrooms}
                                type="number"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                label="SỐ PHÒNG TẮM:"
                            />
                        </div>
                        <div className='px-4 py-2'>
                            <InputField
                                required={true}
                                name="kitchens"
                                value={values.kitchens}
                                type="number"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                label="SỐ PHÒNG BẾP:"
                            />
                        </div>
                    </div>
                    {/* <div className='bg-white mb-20 border-gray-300 border relative'>
                        <div className='p-4 border-gray-300 border-b'>
                            <h2 className='font-bold text-xl pb-2'>Tiện nghi</h2>
                            <p className='text-sm text-gray-600'>Rất nhiều khách hàng đã tìm kiếm chỗ nghỉ dựa trên các tiêu chí về tiện nghi.</p>
                        </div>

                    </div> */}
                    <div className='bg-white mb-20 border-gray-300 border relative'>
                        <div className='p-4 border-gray-300 border-b'>
                            <h2 className='font-bold text-xl pb-2'>Nội quy chỗ nghỉ</h2>
                        </div>
                        <div className='px-4 py-2'>
                            <CustomEditor
                                handleChange={onEditorChange("homeRule")}
                            />
                        </div>
                    </div>
                    <div className='bg-white mb-20 border-gray-300 border relative'>
                        <div className='p-4 border-gray-300 border-b'>
                            <h2 className='font-bold text-xl pb-2'>Thông tin chỗ nghỉ</h2>
                            <p className='text-sm text-gray-600'>Chia sẻ với khách hàng một vài thông tin ngắn gọn và nổi bật về chỗ nghỉ này của bạn.</p>
                        </div>
                        <div className='px-4 py-2'>
                            <CustomEditor
                                handleChange={onEditorChange("homeDescription")}
                            />
                        </div>
                    </div>
                    <div className='bg-white mb-20 border-gray-300 border relative'>
                        <div className='p-4 border-gray-300 border-b'>
                            <h2 className='font-bold text-xl pb-2'>Hình ảnh chỗ nghỉ</h2>
                            <p className='text-sm text-gray-600'>Một vài tấm ảnh đẹp sẽ giúp khách hàng có nhiều thiện cảm hơn về chỗ nghỉ của bạn.</p>
                        </div>
                        <div className='px-4 py-2'>
                            <div className='d-flex flex-wrap'>
                                {previewImages && previewImages.map((previewImage) => (
                                    <div className='previewImage'>
                                        <img src={previewImage} />
                                    </div>
                                ))}
                                <div>
                                    <Dropzone isPreview={false} handleUpload={handleUpload} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='bg-white mb-10 border-gray-300 border relative'>
                        <div className='p-4 border-gray-300 border-b'>
                            <h2 className='font-bold text-xl pb-2'>Thiết lập giá</h2>
                            <p className='text-sm text-gray-600'>Một vài tấm ảnh đẹp sẽ giúp khách hàng có nhiều thiện cảm hơn về chỗ nghỉ của bạn.</p>
                        </div>
                        <div className='px-4 pt-4 pb-2 '>
                            <InputField
                                name="homeFeeWeekday"
                                value={values.homeFeeWeekday}
                                type="number"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                label="THỨ 2 - THỨ 6"
                                required={true}
                            />

                        </div>
                        <div className='px-4 py-2 '>
                            <InputField
                                required={true}
                                name="homeFeeWeekend"
                                value={values.homeFeeWeekend}
                                type="number"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                label="THỨ 7 - CHỦ NHẬT"
                            />
                        </div>
                    </div>
                </div>
                <div className='col-4'>

                </div>
            </div>
            <div>
                <button className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded'>
                    Tạo phòng
                </button>
            </div>
        </form>
    )
}

export default InformationStep
