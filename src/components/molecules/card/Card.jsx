import React from "react";
import { numberWithCommas } from "src/utils/helper";
import "./Card.scss";
function Card({ src, title, price, description, onClick }) {
  return (
    <div className="col-xs-6 col-md-3 col-lg-20" onClick={onClick}>
      <div className="promo is-relative">
        <a href="#">
          <div className="promo_image">
            <div className="promo_image1">
              <img src={src} alt={title} className="w--100" />
            </div>
          </div>
        </a>
        <div className="promo_content">
          <div className="title_small">
            <div className="text_left">{title}</div>
            <div className="text_right">
              <i className="fas fa-star"></i>
              5
            </div>
          </div>
          <div className="is-relative">
            <div className="promo_title">
              <i className="fas fa-bolt"></i>
              {description}
            </div>
            <div className="promo_price">
              {numberWithCommas(price)}<sup>đ</sup>/ đêm
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Card;
