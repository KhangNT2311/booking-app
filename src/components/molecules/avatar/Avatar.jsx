import React from 'react'
import Circle from 'src/components/atoms/circle/Circle';

function Avatar({ onClick, url = "", name }) {
    const nameArr = name.split(" ")
    const firstLetter = nameArr[nameArr.length - 1][0];
    return (
        <div onClick={onClick} className='cursor-pointer pt-2 pl-1 pb-2 pr-3 bg-gray-100 rounded-full'>
            <div className='d-flex items-center'>
                {url ?
                    <img src={url} /> : <Circle letter={firstLetter} />
                }
                <p>{name}</p>
            </div>
        </div>
    )
}

export default Avatar
