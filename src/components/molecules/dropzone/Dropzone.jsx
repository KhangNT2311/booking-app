import React, { useEffect, useState } from 'react'
import ReactDropzone from "react-dropzone";
import './Dropzone.scss'
const Dropzone = ({ isPreview = true, defaultValue, name, handleUpload, error, errorText }) => {
    const [fileNames, setFileNames] = useState([]);
    const [filePreview, setFilePreview] = useState('');
    useEffect(() => {
        if (defaultValue) {
            setFilePreview(defaultValue)
        }
    }, [defaultValue])
    const handleDrop = (acceptedFiles) => {
        setFileNames(acceptedFiles.map(file => file.name))
        const [file] = acceptedFiles;
        if (file) {
            const url = URL.createObjectURL(file)
            setFilePreview(url)
        }
        handleUpload(file);
    };

    return (
        <>
            <div className={`customDropzone ${error ? "error" : ""}`}>
                <ReactDropzone noKeyboard={true} onDrop={handleDrop} multiple={false}>
                    {({ getRootProps, getInputProps }) => {
                        return (
                            <div {...getRootProps({ className: "dropzone" })}>
                                <input  {...getInputProps()} />
                                <p >+</p>
                            </div>
                        );
                    }}
                </ReactDropzone>
                <div>
                    {isPreview && filePreview && <img src={filePreview} alt="National ID" />}
                </div>
            </div>
            {error && <p className="error">{errorText}</p>}
        </>)
}

export default Dropzone
