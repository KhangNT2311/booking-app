import React from "react";
import "./SimpleCard.scss";
const SimpleCard = ({ src, title, description }) => {
  return (
    <div className="simpleCard">
      <div className="simpleCard__wrapper">
        <div className="simpleCard__img">
          <img src={src} alt="simple card" />
        </div>
        <h4>{title}</h4>
        <p>{description}</p>
      </div>
    </div>
  );
};

export default SimpleCard;
