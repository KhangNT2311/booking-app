import React from 'react'
import { Link } from 'react-router-dom'
import RoutesString from 'src/routes/routesString'
import './CheckoutHeader.scss'
function CheckoutHeader() {
    return (
        <div className='checkoutHeader'>
            <div className="container2 container--sm w--100">
                <div className="row middle-xs">
                    <div className="col-xs-12 is-flex">
                        <Link to={RoutesString.HOME} className="logo_header">
                            <img src="	https://www.luxstay.com/logo@2x.png"/>
                        </Link>
                        <div className="checkout_progress">
                            <span className="info_book">1. Thông tin đặt chỗ</span>
                            <i class="fas fa-angle-right"></i>
                            <span className="checkout_payment">2. Xác nhận và thanh toán</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CheckoutHeader
