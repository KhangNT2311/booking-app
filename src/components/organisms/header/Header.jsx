import React from 'react';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import Avatar from 'src/components/molecules/avatar/Avatar';
import RoutesString from 'src/routes/routesString';
import { logout } from 'src/store/slices/authSlice';
import './Header.scss';
function Header({ menu }) {
  const user = useSelector(state => state.auth.user);
  const dispatch = useDispatch()
  const [isOpen, setIsOpen] = useState(false);
  const renderItems = (items) => items?.map((item) => {
    return (
      <div class='right_item'>
        <NavLink to={item.href} className='login_link'>
          {item.label}
        </NavLink>
      </div>
    );
  })
  const handleClick = () => {
    setIsOpen(value => !value)
  }
  const handleLogout = () => {
    dispatch(logout())
  }
  const authItems = [{
    label: 'Đăng ký',
    href: RoutesString.SIGN_UP,
  },
  {
    label: 'Đăng nhập',
    href: RoutesString.LOGIN,
  },]
  return (
    <div className='header'>
      <div className='header_login'>
        <Link className='logo' to={RoutesString.HOME}>
          <img
            src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQDxUQEBAWFRAQFRAVFRUVFxUVFRAQFRUWFhUVFRUYHSggGBomHRUfITEhJSkrLi4uICszODMwQzAwMDABCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAJYBLAMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAQYEBQcIAgP/xABKEAABAwIDBQQGBQgFDQAAAAABAAIDBBEFEiEGBzFBURMyYXEUIiNCgbEVM1JykRY1YnWCobPB
           JDZVhLQIJTRDU2ODkpOU0dLT/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AO4oiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIihBKKEQSihEEooRBKKEQSii6XQSihEEooRBKKEQSihEEooRBKKEQSihEEoiICIiAiIgIqxtNt9huHXbUVLe1H+qZ7ST4tb3f2rKg1+/+mafYUEr29ZJGRH8Gh6DsqLikH+UFGT7TDXNbzLZw4/gY2/NW/Z7e3hFYQztjBIeDagBgP7YJZ+JQXxFDXAgEG4PAj
           UEeClAUKVCAiIgIiICIiAiIgIiICIoQSiIgIiICIiAiIgIiICIiCUREBERB8vcGgkkAAEknQADiSVwHedvfkmc+kwx5ZALtfUDR83IiM+6zx4nwW53/AO2jomDC6d1nytDqgg6iI92LwzWufC3VcDQS5xJJJuTqSdSSvqONzjZrST0Av8l1HYjdvTtpfpTG5Oxo7B0cRJa6UHul1vWseTW+sfnsKvfJS0nssJwuKONugfIA3MOvZs1+JcSg5BLTvZ32Ob5gj5r8l1+m36zPOWsw+nliOhazM02/bzg+SzKzZHCMfgfUYMRT1sYzPpnWYHeBZezb8ntOXr4BTN3+8qrwp4YXGajJ9aFx7o5mInunw4H969N4DjUFdT
           sqaZ+eKQaHm082uHJw5heM62kkhkdFKwskjcWva4WLXDQghXrc5to7Dq0QyO/olU5rJAeEch0ZKOnQ+HkEHqJQpUICKAbqUBEUXQSiJdAREQEREBERAREQEREBERAREQEREBERBKIiAvmR4a0ucbBoJJ6Aakr6Wm2zeW4ZWOHFtJVkeYheQg8lbVYw6trp6t17zyPcAdcrL2Y34NAHwWDQ1HZSskyNf2bmuyPF2PykHK4cwbahfgiDvGIVFHtdTMZHOabEacOLYHm8TyR6xA94ad4at1uFxnH8CqaGc09VEY5G8jwcPtNdwc3xCwaed8b2yRuLXsIc1zSQ5rhwII1BXW8A3g0eKQNw/H2AnhFWCzSxx0BeR3D+kND7
           w4lByJjSSABckgAdSeAXZtidjGYIG4vi9Sad7ATFTsd7R5I7rwO8SD3B8SNQqft9u3qcM9tGe3oXWLJ2C+UHuiQDu/e4H9yqNdiM8+Tt5nydk0MZnc52SMcGtudAOiDfbxdqm4rXGqZTthblawW1fIG3s+Q8C62mnAADWyq6Ig9d7s8ZNbhFNO43fkyPPMvjJYT8ct/irOFy/wDyd5ScIe08GVMoHkWRu+ZXUAgpe5x5dgdMXEk+21JufrX8yroqTuZ/MdN/xv4z1dkBUWkefyqmbc5RhsZtc2B7dutlelQ6P+tc/wCrI/47UF8VIhkd+VL23OX6LBtc2v6S3W3C6u6o0H9apP1W3/EtQXOsp+1ifHmcztGubmYcr2Z
           hbM08nDiCqtsNjM2eXC65166itZ509MpCfZzt6nk7ofElW9VLb3AppWx11ELYhQEvi/38R+sp3dQ4Xt49LoP026x2WFkdHR2OI1xMcA4iFtvaVD+jWDXz5HVb3CKI09PHC6V8ro2hpkkN3yO5ucfErS7JmirT9MQNPb1EbYnFziXQBnehDToyzhrYC/HmrMgrW2+0EtJHFFSsa+trZRDTtd3Gutd0j7e40an4LUt2IxBwzy47VeknW8YYyFp6CHmPiLrcbabOyVjYpaaURVtFIZaeRwuzMRZ8cg+w4aG2vyWpi3gOpSI8Yo5KN+g7doM1JIeRErLlt/skacygytkMaqhUy4ZiBa6rp2MljmYMraulccoky+64HQgaX/F
           WupizsczM5udrm5mmzm3FszTyI4hYlE2lnc2th7ORzo+zbOwtcTFmzZA8cr626rPQUgbuz/bGJ/8AcN/+ar+wWzE2IYdDVzYviLZJe1zBk4DRklewWBaTwaOa6uFS9zf5jpf7x/HlQWLAMJ9Eh7H0iaeznHtJ3h8mvLMANAtkixcVr2U0ElRIbRwsfI77rQT/ACQc6292tqoMRaaYn0TCxBJXgHvtqXtYGEcy1pzfE9F02N4cA5pu1wBBHAg6grlmw2J4Y/D53V9dTCoxZ08tSx00QcxkoLWRd64ys5ciSt9ukxgTUBpjK2WTDpH0xewhzZYmE9jI0ji0s0B8Cgu6pWG4hNQ4q+hqpXPpq8vmopXm5ZINZaUnw4t8NNSVd
           Vots9nxiFI6EOyTMLZaeUaOhqWaxvB5a6HwJQNtNoW4fSOmy55nERwRDUzVL9I2Ac9dT4Ar9NkcOqKejYyrndNUuu+V7je0j9XMZ0a3gPJUvYc1OL1YxCuYGNwy9PFECHNNeABUTkDhyDRra/HS56aglERAWFjVF6RSzQf7aKaPyzsLf5rNRB4fljLHFrhZzSQQeRBsQvhdC327MmixR8zW+wrS6Zh5CQn2rfPMc3k4KjYdWOgmjmYGl0T2PAcA5pLSCA5p4jTggvuw+7B9TH6diL/RcPYA4uf6r5Wfo5u60/aPG+gKqO1jaEVkgw50hpb+oZe9fnbmW34X1txXZ9p6cbVYbHPQVBbUU31lG59mmQjgRwzccrzoRcaa24biu
            FVFJIYqmF8Ug917S0+YvxHiNEFt2B3k1OG+wlHpFA+4fA/XK12juzJ4fdOh/erJtFu8pMSgOI7PvDm8ZKQmzo3cSGA90/oHQ+6eAXJIYnPcGsaXOcbBrQSSegA4rs26vYmpw5xxbEZnUVPE0kxklrpW9Jm8m34MPrE206hxqeFzHFj2lr2khzXAgtI4gg8CvzVp3k7UtxTEH1McYZEAGR6APext7PkI4uN/gLDldafZ/CJa2qipYReSZ4aP0R7zj4AXPwQekNxWHmHBI3EWM75pfgXZG/uYCugrFwqgZTU8dPGLMhYyNv3WgAfJZSClbmh/mOl8pv4z1dVQ4t1NAwZY5qxjBezWVL2tbc30CyqLdvSRSslbU1hdG5rgHVLy0kG
           4BHMeCC5Ln2NVLcP2hirKg5aWupfRRKdGRVDZBI0PdwaHAWBP8iugrFxPDoaqJ0FRE2SJ+jmOFwf/AAfFBkF4AzEjKBe99Lcb36KibLztrscqsQgOalgp46Jsg7s8oeJZDGebW2AvzvopG6XDO6TUGG9+wM8nZeWXjb4q60FFFBE2GGNscUYs1jAA1o8AEH7qq4NtBPW4jK2ny/R1HeKSQi5qazmyN17BrBxPM+d1vcZo3z08kMczoXyNLRK0AujvxLb87X15cV84Dg8NFTR0sDbRQtDR1ceLnO6uJJJPUoKdiQ+hcR9MbphmIva2qHu0lYdGVHgx/Bx669AugLGxPD4qmF9PMwPilaWPaebT8jzB5LF2bwp1HSx0z
            p3TdiC1r3gBxjB9RptxsLC/ggw8X2hNNiFJSvYBFWidolJtlnYGlsduF3A6dTwW9kYHAtcAWuBBBFwQdCCDxCwcewSnroHU9TGHxusbagtcODmuGrXDqFVhsJWNHZx47Wtg+ycj5AOgmIzBBh4dSRUW0QpqD1IKimkmq4GfVQyA2ika3hG53DKLaWXRVpNmNl6bD2OEAc6SU5pZpHF8s7+r3njx4cNVu0BUvc3+YqX+8f4iVXRajZPAW4fRR0bHl7Yc9nOABdne5+oH3rINuqPvHJqpKTCGE/06XPPb3aKCz5LkcMxAaOpV4Wmptn2txGbEHSOfJLFFAxpAAghaczmttxzP9Y3QfX5K4d/Z9L/0Iv8A1VYraWPCsZpp4
            Y2xUmJN9DlZG0MY2qaS+mflbpmdcsV+Wn2s2fZiNI6me5zMxY5kje9FIxwc17fG4QbhF8xtIaATcgAE8Mx5mw4L6QUbdJ/o9Z+sq75sV5Wl2W2fbQRysbIX9vUT1BJAGUykXaLchZbpBKIiAiIgr+3GysOK0bqaXR3eiktcxSgaOHUciOYK8n7RYDUYfUOpqmMskZ/yvbyew82nqvaC0e1eylHicPY1UWa18jxpJEerHcvLgeYQeRsHxeoo5hPTSuilbwc02uOhHBw8DoumUO/KoMfZ1tFDUDme5m8S0hzb+QCx9qNyWIU7i6jLamLkAQyUDxa42PwPwVAr9n62nNp6SaP78T2g+RI1QdOO+9sQPoeFQQuI43FviGNbf8Vz7avb
           OuxN4dVzEtabtjb6sbD4MHE+JufFaulwuolOWKCV7jyYx7j+ACuGz26TFqsgug9HjPF8/qEDwj79/MDzQUaGJz3BjGlz3EBrQLlzjoAAOJXpXc7u8+jYjVVLR6bM21uPo8R1yA/aPM/DrfZ7Bbs6LCrSfXVVtZngep1Ebfc89T4q8IChSoQEREBERAREQEREBERAREQEREBERAREQEREBERAREQSiIgIiICIiAiIgIiICIiAoUqEBERAREQEREBERAREQEQIgIiICIiAiIgIiICIiAiIglERAREQEREBERAREQEREBQpRBCKUQQilEEIpRBCWUoghFKIIRSiCEUoghFKIIRSiCEUoghFKIIRSiAiIgIiICIiAiIgIiICIiAiIgIiICI
            iAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiIP/Z'
            alt=''
          />
        </Link>
        <ul class='header_right'>
          {renderItems(menu)}
          {user ? <div className='right_item avatarWrapper relative'><Avatar onClick={handleClick} name={user.name} />
            <div className={`userDropdown ${isOpen ? "open" : ""}`}>
              <Link to={RoutesString.CLIENT_ACCOUNT_SETTINGS} className=" userDropdown__item">
                Cài đặt tài khoản
              </Link>
              <div onClick={handleLogout} className="userDropdown__item">
                Đăng xuất
              </div>
            </div>
          </div> : renderItems(authItems)}
        </ul>
      </div>
    </div>
  );
}

export default Header;
