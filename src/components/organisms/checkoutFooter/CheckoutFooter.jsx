import React from 'react'
import './CheckoutFooter.scss'
function CheckoutFooter() {
    return (
        <div className="footer_payment">
            <div className="footer_bottom">
                <div className="container1 container-md">
                    <div className="row footer_center">
                        <div className="col-xs-12 text-center1">
                            <p className="footer_text1">
                            © 2021 Luxstay. Bản quyền thuộc về Công ty TNHH Luxstay
                             Việt Nam - MSDN: 0108308449. Mọi hành vi sao chép đều là
                              phạm pháp nếu không có sự cho phép bằng văn bản của chúng tôi.
                            </p>
                            <p className="footer_text2">
                                Tầng 21 tòa nhà Capital Tower số 109
                             phố Trần Hưng Đạo, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội. 
                             Email: info@luxstay.com, Số điện thoại: 18006586.</p>
                            <p className="footer_text2">
                            Số Giấy chứng nhận đăng ký doanh nghiệp: 0108308449,
                             ngày cấp: 06/06/2018, nơi cấp: Sở Kế hoạch và Đầu tư TP Hà Nội
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CheckoutFooter
