import React from 'react'
import { Input } from 'reactstrap'
function InputField({ required,label, name, type, options, ...props }) {
    const renderOptions = (options) => options.map((item) => <option value={item.value}>{item.label}</option>)
    return (
        <div className="">
            <label htmlFor={name} className="block text-xs font-medium text-gray-700">
                {label} {required && <span className='text-red-600'>*</span>}
            </label>
            <Input
                name={name}
                id={name}
                type={type}
                required={required}
                className="p-2 mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                {...props}
            >
                {/* {null} */}
                {type === 'select' && options && options.length > 0 && renderOptions(options)||null}
            </Input>
        </div>
    )
}

export default InputField
