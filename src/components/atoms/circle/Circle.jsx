import React from 'react'

function Circle({ letter, className = "", src = "" }) {
    return (
        <>
            {src ?
                <img className={`w-6 h-6 d-flex justify-center items-center rounded-full bg-red-200  mr-2 ${className}`} src={src} alt="circle" />
                :
                <span className={`w-6 h-6 d-flex justify-center items-center rounded-full bg-red-200  mr-2 ${className}`}>{letter}</span>
            }
        </>
    )
}

export default Circle
