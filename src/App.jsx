import { BrowserRouter } from "react-router-dom";
import "./App.scss";
import Routes from "./routes/Routes";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ThemeConfig from "./pages/admin/theme";
import ScrollToTop from "./pages/admin/components/ScrollToTop";
import GlobalStyles from "./pages/admin/theme/globalStyles";
import { BaseOptionChartStyle } from "./pages/admin/components/charts/BaseOptionChart";

function App() {
  return (
    <>
      <ThemeConfig>
        <ScrollToTop />
        <GlobalStyles />
        <BaseOptionChartStyle />
        <BrowserRouter>
          <Routes />
        </BrowserRouter>
        <ToastContainer autoClose={5000} />
      </ThemeConfig>

    </>
  );
}

export default App;
