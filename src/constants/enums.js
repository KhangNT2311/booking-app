export const USER_ROLE = {
  USER: 'user',
  ADMIN: 'admin',
  HOST: 'host',
};
export const METHOD = {
  GET: "GET",
  POST: "POST",
  PUT: "PUT",
  DELETE: "DELETE"
}
export const RULE = {
  ALL_DAYS: 0,
  WEEKDAY: 1,
  WEEKEND: 2,
  CUSTOM_DATE: 3,
};
export const STATUS = {
  ACTIVE: 'active',
  INACTIVE: 'inactive',
  PENDING: 'pending',
  VOID: 'void'
}
export const STATUSES = Object.values(STATUS)
export const RULES = Object.values(RULE);