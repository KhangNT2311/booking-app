import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import RoutesString from "./routesString";

const RoleRoute = ({ children, requireRoles = [] }) => {
  const history = useHistory();
  const user = useSelector((state) => state.auth.user);
  const role = user?.role;
  console.log("user",user);
  useEffect(() => {
    if (!role || requireRoles.length === 0) return;

    const checkRole = requireRoles.includes(role);

    if (!checkRole) {
      history.replace(RoutesString.ERROR_404);
    }
  }, [history, role, requireRoles]);

  return <>{children}</>;
};

export default RoleRoute;
