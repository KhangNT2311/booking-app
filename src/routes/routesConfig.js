import { lazy } from 'react';
import { Redirect } from 'react-router-dom';
import { USER_ROLE } from 'src/constants/enums';
import AdminGuard from 'src/guards/admin/AdminGuard';
import AdminLoginGuard from 'src/guards/adminLogin/AdminLoginGuard';
import HostGuard from 'src/guards/host/HostGuard';
import HostLoginGuard from 'src/guards/hostLogin/HostLoginGuard';
import LoginGuard from 'src/guards/login/LoginGuard';
import RoutesString from './routesString';

const HomePage = lazy(() => import('../pages/client/home/HomePage'));
const LoginPage = lazy(() => import('../pages/client/login/LoginPage'));
const LocationPage = lazy(() => import('../pages/client/location/LocationPage'));
const RoomPage = lazy(() => import('../pages/client/room/RoomPage'));
const ClientACcountSettingsPage = lazy(() => import('../pages/client/clientSettings/ClientSettingsPage'));
const PaymentSuccessPage = lazy(() => import('../pages/client/paymentSuccess/PaymentSuccess'));
const HostLoginPage = lazy(() => import('../pages/host/login/HostLoginPage'));
const CheckoutInformationPage = lazy(() => import('../pages/client/checkoutInformation/CheckoutInformationPage'));
const PaymentPage = lazy(() => import('../pages/client/payment/PaymentPage'));
const GetStartedPage = lazy(() => import('../pages/host/getStarted/GetStartedPage'));
const InformationStepPage = lazy(() => import('../pages/host/createHome/informationStep/InformationStep'));
const InformationStepUpdatePage = lazy(() => import('../pages/host/createHome/informationStepUpdate/InformationStepUpdate'));
const HostAccountSettingsPage = lazy(() => import('../pages/host/accountSettings/HostAccountSettingsPage'));
const DashboardPage = lazy(() => import('../pages/admin/pages/DashboardApp'));
const AdminUserPage = lazy(() => import('../pages/admin/pages/User'));
const AdminHomesPage = lazy(() => import('../pages/admin/pages/Home'));
const AdminProductsPage = lazy(() => import('../pages/admin/pages/Products'));
const AdminHomeTypePage = lazy(() => import('../pages/admin/pages/HomeType'));
const AdminBlogPage = lazy(() => import('../pages/admin/pages/Blog'));
const AdminLoginPage = lazy(() => import('../pages/admin/pages/Login'));
const AdminRegisterPage = lazy(() => import('../pages/admin/pages/Register'));
const AdminCreateHomeTypePage = lazy(() => import('../pages/admin/pages/CreateHomeType'));
const HostHomeManagement = lazy(() => import('../pages/host/homeManagement/HostHomeManagement'));
const MainLayout = lazy(() => import('../layouts/main/MainLayout'));
const HostLayout = lazy(() => import('../layouts/host/HostLayout'));
const CheckoutLayout = lazy(() => import('../layouts/checkout/CheckoutLayout'));
const CreateHomeLayout = lazy(() => import('../layouts/createHome/CreateHomeLayout'));
const DashboardLayout = lazy(() => import('../pages/admin/layouts/dashboard'));
const LogoOnlyLayout = lazy(() => import('../pages/admin/layouts/LogoOnlyLayout'));

export const routesConfig = [

  {
    page: GetStartedPage,
    path: RoutesString.GET_STARTED,
    requireRoles: [USER_ROLE.HOST],
    exact: true,
  },

  {
    layout: LogoOnlyLayout,
    path: RoutesString.ADMIN_AUTH,
    guard: AdminLoginGuard,
    routes: [{
      page: AdminLoginPage,
      path: RoutesString.ADMIN_LOGIN,
      exact: true
    }, {
      page: AdminRegisterPage,
      path: RoutesString.ADMIN_REGISTER,
      exact: true
    }]
  },
  {
    layout: DashboardLayout,
    path: RoutesString.ADMIN,
    guard: AdminGuard,

    routes: [{
      page: DashboardPage,
      path: RoutesString.DASHBOARD,
      exact: true,
      requireRoles: [USER_ROLE.ADMIN],
    }, {
      page: AdminHomesPage,
      path: RoutesString.ADMIN_HOMES,
      exact: true,
      requireRoles: [USER_ROLE.ADMIN],
    }, {
      page: AdminUserPage,
      path: RoutesString.ADMIN_USER,
      exact: true,
      requireRoles: [USER_ROLE.ADMIN],

    },
    {
      page: AdminHomeTypePage,
      path: RoutesString.ADMIN_HOME_TYPES,
      exact: true,
      requireRoles: [USER_ROLE.ADMIN],

    }, {
      page: AdminProductsPage,
      path: RoutesString.ADMIN_PRODUCTS,
      exact: true,
      requireRoles: [USER_ROLE.ADMIN],

    }, {
      page: AdminCreateHomeTypePage,
      path: RoutesString.ADMIN_HOME_TYPE_CREATE,
      exact: true,
      requireRoles: [USER_ROLE.ADMIN],

    }, {
      page: AdminCreateHomeTypePage,
      path: RoutesString.ADMIN_HOME_TYPE_UPDATE,
      requireRoles: [USER_ROLE.ADMIN],

    }, {
      page: AdminBlogPage,
      path: RoutesString.ADMIN_BLOG,
      exact: true,
      requireRoles: [USER_ROLE.ADMIN],

    }]
  },
  {
    layout: CreateHomeLayout,
    path: RoutesString.CREATE_HOME,
    routes: [{
      page: InformationStepPage,
      path: RoutesString.INFOMATION_STEP,
    }, {
      page: InformationStepUpdatePage,
      path: RoutesString.INFOMATION_STEP_UPDATE,
    },]
  },

  {
    layout: HostLayout,
    path: RoutesString.HOST,
    guard: HostGuard,
    routes: [
      {
        page: HostAccountSettingsPage,
        path: RoutesString.HOST_ACCOUNT_SETTINGS,
        exact: true,
        requireRoles: [USER_ROLE.HOST],

      },
      {
        page: HostHomeManagement,
        path: RoutesString.HOST_HOME_MANAGEMENT,
        exact: true,
        requireRoles: [USER_ROLE.HOST],

      }
    ]
  },
  {
    layout: CheckoutLayout,
    path: RoutesString.CHECKOUT,
    routes: [
      {
        page: PaymentPage,
        path: RoutesString.CHECKOUT_PAYMENT,
      },
      {
        page: CheckoutInformationPage,
        path: RoutesString.CHECKOUT_INFORMATION,
      }
    ]
  },
  {
    layout: MainLayout,
    path: RoutesString.HOST_AUTH,
    guard: HostLoginGuard,
    routes: [
      {
        page: HostLoginPage,
        path: RoutesString.HOST_LOGIN,
      },
    ]
  },
  {
    layout: MainLayout,
    path: RoutesString.LOGIN,
    guard: LoginGuard,
    routes: [
      {
        page: LoginPage,
        path: RoutesString.LOGIN,
      },
    ]
  },
  // {
  //   layout: MainLayout,
  //   path: RoutesString.CLIENT_ACCOUNT_SETTINGS,
  //   guard:Client,
  //   routes: []
  // },
  {
    layout: MainLayout,
    path: RoutesString.MAIN_LAYOUT,
    routes: [
      {
        page: HomePage,
        path: RoutesString.HOME,
        exact: true,
      },

      {
        page: LocationPage,
        path: RoutesString.LOCATION,
        exact: true,
      },
      {
        page: RoomPage,
        path: RoutesString.ROOM,
      },

      {
        page: PaymentSuccessPage,
        path: RoutesString.PAYMENT_SUCCESS,
      },
      {
        page: HostLoginPage,
        path: RoutesString.HOST_LOGIN,
        exact: true,
      },
      {
        page: ClientACcountSettingsPage,
        path: RoutesString.CLIENT_ACCOUNT_SETTINGS,
        exact: true,
        requireRoles: [USER_ROLE.USER,USER_ROLE.HOST],
      },
      {
        page: () => <Redirect to={RoutesString.ERROR_404} />,
      },
    ],
  },

];
