import axios from 'axios'
import { METHOD } from 'src/constants/enums';
import { refreshTokenSuccess } from 'src/store/slices/authSlice';
import store from "../store";
import config from './config';
const newInstance = axios.create();
let isExpired = false;
const handleRefreshToken = async () => {
  const { auth } = store.getState();
  const response = await newInstance({
    url: config.API.REFRESH_TOKEN_SERVICE,
    method: METHOD.POST,
    data: {
      email: auth?.user?.email,
      refreshToken: auth?.token?.refreshToken
    }
  })
  store.dispatch(refreshTokenSuccess(response.data));
  return response.data;
}
// Add a request interceptor
axios.interceptors.request.use(
  async (config) => {
    // store.dispatch({
    //   type: ILoadingType.START_LOADING
    // })
    const newConfig = { ...config };
    console.log("config", config);
    const { auth } = store.getState();
    if (auth.token) {
      const expiredDate = new Date(auth.token.expiresIn);
      let accessToken = auth?.token?.accessToken;
      if (expiredDate.getTime() < Date.now()) {
        const token = await handleRefreshToken()
        console.log("token", token);
        accessToken = token.accessToken;
      }
      if (newConfig.hasToken) {
        newConfig.headers['Authorization'] = `Bearer ${accessToken}`

      }
    }
    // if (newConfig.url?.includes("/CMS/GraphQL")) {

    //   newConfig.headers['token'] = auth?.cmsToken
    //   return newConfig;
    // } else {
    //   const token = auth?.accessToken || '';
    //   if (token) {
    //     newConfig.headers[`token`] = token;
    //   }
    //   return newConfig;
    // }
    return newConfig;
  },
  function (error) {
    // store.dispatch({
    //   type: ILoadingType.START_LOADING
    // })
    // Do something with request error
    return Promise.reject(error)
  }
)

// Add a response interceptor
axios.interceptors.response.use(
  function (response) {
    // store.dispatch({
    //   type: ILoadingType.FINISH_LOADING
    // })

    return response
  },
  function (error) {
    if (error.response.data.code === 401 && error.response.data.message === "jwt expired") {
      console.log("go hereee")
      const { auth } = store.getState();
      handleRefreshToken(auth?.user?.email, auth?.token?.refreshToken)(store.dispatch)
    }
    // store.dispatch({
    //   type: ILoadingType.FINISH_LOADING
    // })

    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error)
  }
)
