import axios from 'axios'
import { stringify, parse } from 'query-string'

import './config'
import './interceptors'
import store from ".././store";

export const API_ERROR_MESSAGE_GENERAL = 'Oops. Something wrong happened'
export const ERROR_MESSAGE_NO_NETWORK = 'OFFLINE_MESSAGE'

let isOnline = navigator.onLine
window.addEventListener('offline', () => {
  isOnline = false
})

window.addEventListener('online', () => {
  isOnline = true
})

const API = async ({
  url,
  params = '',
  method = 'get',
  headers = {},
  data = '',
  cancelTokenSource,
  hasToken=false,
  ...props
}) => {
  const newParams = parse(stringify(params , { arrayFormat: 'comma' }))
  // if(hasToken){
  //   const { auth } = store.getState();
  //   headers['Authorization'] = `Bearer ${auth?.token?.accessToken}`

  // }
  try {
    const response = await axios({
      method,
      url,
      headers: {
        ...headers,
      },
      ...props,
      params: newParams,
      data,
      cancelToken: cancelTokenSource?.token,
      hasToken,
    })

    return response && response.data
  } catch (error) {
    if (isOnline) {
      throw error
    } else {
      const offlineResponse = {
        response: {
          data: {
            error: {
              message: ERROR_MESSAGE_NO_NETWORK
            }
          },
        },
      }

      throw offlineResponse
    }
  }
}

export default API
