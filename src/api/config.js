
const REACT_APP_API_URL = process.env.REACT_APP_API_URL_V1 || 'http://localhost:3000/api'

const config = {
    API: {
        LOGIN_SERVICE: '/auth/login',
        REGISTER_SERVICE: '/auth/register',
        REFRESH_TOKEN_SERVICE: "/auth/refresh-token",
        SECURE_URL_SERVICE: "/settings/getSecureS3Url",
        PROFILE_SERVICE: "/users/profile",
        HOME_TYPE_SERVICE: "/homeTypes",
        HOME_SERVICE: "/homes",
        STRIPE_SESSION_SERVICE: "/settings/stripe-session"
    }
};

Object.keys(config.API).forEach(item => config.API[item] = REACT_APP_API_URL + config.API[item])

export default config