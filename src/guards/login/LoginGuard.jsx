import React from 'react'
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import RoutesString from 'src/routes/routesString';

function LoginGuard({ children }) {
    const accessToken = useSelector((state) => state?.auth);
    if (accessToken?.token?.accessToken) return <Redirect to={RoutesString.HOME} />
    return (
        children
    )
}

export default LoginGuard
