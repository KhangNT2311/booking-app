import React from 'react'
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { USER_ROLE } from 'src/constants/enums';
import RoutesString from 'src/routes/routesString';

function HostGuard({ children }) {
    const auth = useSelector((state) => state?.auth);
    if (!auth?.token?.accessToken || auth?.user?.role !== USER_ROLE.HOST) return <Redirect to={RoutesString.HOST_LOGIN} />
    return (
        children
    )
}

export default HostGuard
