import API from "src/api";
import config from "src/api/config";
import { METHOD } from "src/constants/enums";



export const getSecureS3Url = async () => {
    const response = await API({
        url: config.API.SECURE_URL_SERVICE,
        method: METHOD.GET
    })
    return response?.secureUrl;
}
export const uploadS3BucketImage = async (secureUrl, file) => {
    await API({
        url: secureUrl,
        headers: {
            "Content-Type": "multipart/form-data"
        },
        data: file,
        method: METHOD.PUT
    })

    return secureUrl.split("?")?.[0];
}

export const uploadMultipleFiles = async (files) => {
    let promises = [];
    const uploadImage = async (file) => {
        const secureUrl = await getSecureS3Url();
        return await uploadS3BucketImage(secureUrl, file)
    }
    files.forEach((file) => {
        promises.push(uploadImage(file))
    })
    return await Promise.all(promises);
}