import React from 'react'
import HostHeader from 'src/components/organisms/hostHeader/HostHeader'

function HostLayout({ children }) {
    return (
        <div>
            <HostHeader />
            <div className='container-fluid'>
                {children}
            </div>
            <footer>
                footer
            </footer>
        </div>
    )
}

export default HostLayout
