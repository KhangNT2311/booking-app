import React from 'react'
import CheckoutFooter from 'src/components/organisms/checkoutFooter/CheckoutFooter'
import CheckoutHeader from 'src/components/organisms/checkoutHeader/CheckoutHeader'
import './CheckoutLayout.scss'
function CheckoutLayout({ children }) {
    return (
        <div className='checkoutLayout'>
            <CheckoutHeader />
            <div className='checkoutLayout__content'>
                {children}
            </div>
            <CheckoutFooter/>
        </div>
    )
}

export default CheckoutLayout
