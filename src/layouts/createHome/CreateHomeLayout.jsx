import React from 'react'
import { TiArrowLeftOutline } from 'react-icons/ti';
import { useHistory } from 'react-router-dom';
import Logo from 'src/components/atoms/logo/Logo';
import RoutesString from 'src/routes/routesString';
import './CreateHomeLayout.scss';
function CreateHomeLayout({ children }) {
    const { push } = useHistory();
    const handleGoAccountSettings = () => push(RoutesString.HOST_ACCOUNT_SETTINGS)
    
    return (
        <div className='createHomeLayout'>
            <div className="createHomeLayout__header">
                <div className='p-4 cursor-pointer' onClick={handleGoAccountSettings}>
                    <TiArrowLeftOutline className='w-6 h-6' />
                </div>

                <div className="createHomeLayout__header__logo">
                    Logo
                </div>


            </div>
            <div className="createHomeLayout__content">
                {children}
            </div>
        </div>
    )
}

export default CreateHomeLayout
