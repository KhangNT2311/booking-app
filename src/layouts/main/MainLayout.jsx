import React, { useEffect, useState } from 'react';
import { useLocation, useParams } from 'react-router';
import Footer from 'src/components/organisms/footer/Footer';
import Header from 'src/components/organisms/header/Header';
import RoutesString from 'src/routes/routesString';
import { useQueryParam, BooleanParam } from 'use-query-params';
import './MainLayout.scss';
const MainLayout = ({ children }) => {
  const menu = [
    {
      label: 'Host',
      href: RoutesString.HOST_LOGIN,
    },
 
  ];
  return (
    <div className='mainLayout'>
      <div className='mainLayout__header'>
        <Header menu={menu} />
      </div>
      <div className='mainLayout__children'>{children}</div>
      <div className='mainLayout__footer'>
        <Footer />
      </div>
    </div>
  );
};

export default MainLayout;
