import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import app from "./slices/appSlice";
import auth from "./slices/authSlice";
import home from "./slices/homeSlice";
import host from "./slices/hostSlice";
import admin from "./slices/adminSlice";
import client from "./slices/clientSlice";
import storage from "redux-persist/lib/storage";
import { createBrowserHistory } from "history";
import { connectRouter, routerMiddleware } from "connected-react-router";
import persistReducer from "redux-persist/es/persistReducer";

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["auth"],
};
const createRootReducer = (history) =>
  combineReducers({
    app,
    auth,
    home,
    host,
    admin,
    client,
    router: connectRouter(history),
  });
export const history = createBrowserHistory();

const store = configureStore({
  reducer: persistReducer(persistConfig, createRootReducer(history)),
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(routerMiddleware(history)),
});

export default store;
