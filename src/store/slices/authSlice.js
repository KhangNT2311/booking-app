import { createSlice } from "@reduxjs/toolkit";
import API from "src/api";
import config from "src/api/config";
import { METHOD } from "src/constants/enums";
import { toast } from 'react-toastify';
import { push } from "connected-react-router";
import RoutesString from "src/routes/routesString";
import axios from "axios";

const slice = createSlice({
  name: "auth",
  initialState: {
    isLoggedIn: false,
    user: null,
    token: null
  },
  reducers: {
    loginSuccess: (state, action) => {
      state.user = action.payload.user;
      state.token = action.payload.token;
    },
    logoutSuccess: (state, action) => {
      state.user = null;
      state.token = null;
    },
    registerSuccess: (state, action) => {
      state.user = action.payload.user;
      state.token = action.payload.token;
    },
    getProfileSuccess: (state, action) => {
      state.user = { ...state.user, ...action.payload }
    },
    refreshTokenSuccess: (state, action) => {
      console.log("action", action.payload)
      state.token = action.payload;
    },
    updateProfileSuccess: (state, action) => {
      state.user = { ...state.user, ...action.payload }
    }
  },
});
export default slice.reducer;
export const { updateProfileSuccess, refreshTokenSuccess, getProfileSuccess, loginSuccess, logoutSuccess, registerSuccess } = slice.actions;
export const login =
  (values, role = "") =>
    async (dispatch) => {
      const id = toast.loading("Loading...")
      try {
        let url = config.API.LOGIN_SERVICE;
        if (role) {
          url = `${config.API.LOGIN_SERVICE}/${role}`
        }
        const response = await API({
          url,
          method: METHOD.POST,
          data: values
        })
        console.log("Response", response);
        toast.update(id, {
          render: "Đăng nhập thành công!", type: "success", isLoading: false, autoClose: 3000
        });

        setTimeout(() => {
          dispatch(loginSuccess(response))
        }, 2000);
      } catch (e) {
        toast.update(id, {
          render: e?.response?.data?.message, type: "error", isLoading: false, autoClose: 3000
        });
        return console.error(e.message);
      }
    };
export const logout = () => async (dispatch) => {
  try {
    return dispatch(logoutSuccess());
  } catch (e) {
    return console.error(e.message);
  }
};
export const register = (values) => async (dispatch) => {
  try {
    const id = toast.loading("Loading...")

    const response = await API({
      url: config.API.REGISTER_SERVICE,
      method: METHOD.POST,
      data: values
    })
    console.log("Response", response);
    toast.update(id, {
      render: "Tạo tài khoản thành công!", type: "success", isLoading: false, autoClose: 3000
    });

    setTimeout(() => {
      dispatch(registerSuccess(response))
    }, 2000);

  } catch (error) {

  }
}
export const getProfile = () => async (dispatch) => {
  try {
    const response = await API({
      url: config.API.PROFILE_SERVICE,
      method: METHOD.GET,
      hasToken: true
    })
    
    dispatch(getProfileSuccess(response))
  } catch (error) {

  }
}


export const updateProfile = (data) => async (dispatch) => {
  const id = toast.loading("Loading...")

  try {
    const response = await API({
      url: config.API.PROFILE_SERVICE,
      method: METHOD.PUT,
      data,
      hasToken: true
    })
    toast.update(id, {
      render: "Cập nhật thành công!", type: "success", isLoading: false, autoClose: 3000
    });
    dispatch(updateProfileSuccess(response))
  } catch (e) {
    toast.update(id, {
      render: e.response.data.message || "Oops... Có lỗi đang xảy ra", type: "error", isLoading: false, autoClose: 3000
    });
  }
}