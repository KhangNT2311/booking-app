
import { createSlice } from "@reduxjs/toolkit";
import API from "src/api";
import config from "src/api/config";
import { METHOD } from "src/constants/enums";
import { toast } from 'react-toastify';
import RoutesString from "src/routes/routesString";

const slice = createSlice({
    name: "admin",
    initialState: {
        homeTypes: [],
        homes: [],
        homeRebound: true,
        homeTypeRebound: true
    },
    reducers: {
        getHomeTypesSuccess: (state, action) => {
            state.homeTypes = action.payload;
            state.homeTypeRebound = false;

        },
        getHomesSuccess: (state, action) => {
            state.homes = action.payload;
            state.homeRebound = false;
        },
        updateHomeSuccess: (state, action) => {
            state.homeRebound = true;
        },
        updateHomeTypeSuccess: (state, action) => {
            state.homeTypeRebound = true;
        }
    },
});
export default slice.reducer;
const { updateHomeTypeSuccess, updateHomeSuccess, getHomesSuccess, getHomeTypesSuccess } = slice.actions;
export const getHomeTypes =
    async (dispatch) => {
        try {
            const response = await API({
                url: config.API.HOME_TYPE_SERVICE,
                method: METHOD.GET
            })

            dispatch(getHomeTypesSuccess(response));
        } catch (e) {
            return console.error(e.message);
        }
    };

export const getHomes =
    async (dispatch) => {
        try {
            const response = await API({
                url: config.API.HOME_SERVICE,
                method: METHOD.GET,
            })

            dispatch(getHomesSuccess(response));
        } catch (e) {
            return console.error(e.message);
        }
    };

export const updateHome = (id, data) => async (dispatch) => {
    const tid = toast.loading("Loading...")

    try {
        await API({
            url: `${config.API.HOME_SERVICE}/${id}`,
            method: METHOD.PUT,
            data,
            hasToken: true,

        })
        dispatch(updateHomeSuccess())
        toast.update(tid, {
            render: "Cập nhật thành công!", type: "success", isLoading: false, autoClose: 3000
        });
    } catch (error) {
        toast.update(tid, {
            render: error.response.data.message || "Oops... Có lỗi đang xảy ra", type: "error", isLoading: false, autoClose: 3000
        });
        return console.error(error.message);
    }
}

export const updateHomeType = (id, data, push) => async (dispatch) => {
    const tid = toast.loading("Loading...")

    try {
        await API({
            url: `${config.API.HOME_TYPE_SERVICE}/${id}`,
            method: METHOD.PUT,
            data,
            hasToken: true,

        })
        dispatch(updateHomeTypeSuccess())
        toast.update(tid, {
            render: "Cập nhật thành công!", type: "success", isLoading: false, autoClose: 3000
        });
        push(RoutesString.ADMIN_HOME_TYPES)

    } catch (error) {
        toast.update(tid, {
            render: error.response.data.message || "Oops... Có lỗi đang xảy ra", type: "error", isLoading: false, autoClose: 3000
        });
        return console.error(error.message);
    }
}

export const deleteHome = (id) => async (dispatch) => {
    const tid = toast.loading("Loading...")

    try {
        await API({
            url: `${config.API.HOME_SERVICE}/${id}`,
            method: METHOD.DELETE,
            hasToken: true
        })
        dispatch(updateHomeSuccess())
        toast.update(tid, {
            render: "Xoá thành công!", type: "success", isLoading: false, autoClose: 3000
        });
    } catch (error) {
        toast.update(tid, {
            render: error?.response?.data?.message || "Oops... Có lỗi đang xảy ra", type: "error", isLoading: false, autoClose: 3000
        });
        return console.error(error.message);
    }
}

export const deleteHomeType = (id) => async (dispatch) => {
    const tid = toast.loading("Loading...")

    try {
        await API({
            url: `${config.API.HOME_TYPE_SERVICE}/${id}`,
            method: METHOD.DELETE,
            hasToken: true
        })
        dispatch(updateHomeTypeSuccess())
        toast.update(tid, {
            render: "Xoá thành công!", type: "success", isLoading: false, autoClose: 3000
        });

    } catch (error) {
        toast.update(tid, {
            render: error?.response?.data?.message || "Oops... Có lỗi đang xảy ra", type: "error", isLoading: false, autoClose: 3000
        });
        return console.error(error.message);
    }
}

export const createHomeType = (data, push) => async (dispatch) => {
    const tid = toast.loading("Loading...")

    try {
        await API({
            url: config.API.HOME_TYPE_SERVICE,
            method: METHOD.POST,
            data
        })
        toast.update(tid, {
            render: "Tạo thành công!", type: "success", isLoading: false, autoClose: 3000
        });
        dispatch(updateHomeSuccess())
        push(RoutesString.ADMIN_HOME_TYPES)

    } catch (error) {

    }
}