import { createSlice } from "@reduxjs/toolkit";
import API from "src/api";
import config from "src/api/config";
import { METHOD } from "src/constants/enums";
const slice = createSlice({
    name: "home",
    initialState: {
        homeTypes: [],
        homes: [],
    },
    reducers: {
        getHomeTypesSuccess: (state, action) => {
            state.homeTypes = action.payload;
        },

    },
});
export default slice.reducer;
const { getHomeTypesSuccess } = slice.actions;
export const getHomeTypes =
    async (dispatch) => {
        try {
            const response = await API({
                url: config.API.HOME_TYPE_SERVICE,
                method: METHOD.GET
            })

            dispatch(getHomeTypesSuccess(response));
        } catch (e) {
            return console.error(e.message);
        }
    };
