import { createSlice } from "@reduxjs/toolkit";
import API from "src/api";
import config from "src/api/config";
import { METHOD, STATUS } from "src/constants/enums";
const slice = createSlice({
    name: "client",
    initialState: {
        homeTypes: [],
        homes: [],
        provinces: [],
        homeLoading: true,
        home: {},
        currentHomeLoading: true,
    },
    reducers: {
        getHomeTypesSuccess: (state, action) => {
            state.homeTypes = action.payload;
        },
        getListHomesSuccess: (state, action) => {
            state.homes = action.payload;
            state.homeLoading = false
        },
        getListProvincesSuccess: (state, action) => {
            state.provinces = action.payload
        },
        setHomeLoading: (state, action) => {
            state.homeLoading = action.payload
        },
        getHomeSuccess: (state, action) => {
            state.home = action.payload;
            state.currentHomeLoading = false;
        },
        setCurrentHomeLoading: (state, action) => {
            state.currentHomeLoading = action.payload;
        }
    },
});
export default slice.reducer;
const { getHomeSuccess, setCurrentHomeLoading, setHomeLoading, getListProvincesSuccess, getListHomesSuccess, getHomeTypesSuccess } = slice.actions;
export const getHomeTypes =
    async (dispatch) => {
        try {
            const response = await API({
                url: config.API.HOME_TYPE_SERVICE,
                method: METHOD.GET
            })

            dispatch(getHomeTypesSuccess(response));
        } catch (e) {
            return console.error(e.message);
        }
    };

export const getListHomes = (province = "") =>
    async (dispatch) => {
        const params = {
            status: STATUS.ACTIVE,
            province
        }
        try {
            const response = await API({
                url: config.API.HOME_SERVICE,
                method: METHOD.GET,
                params
            })

            dispatch(getListHomesSuccess(response));
        } catch (e) {
            return console.error(e.message);
        }
    };
export const getListProvinces =
    async (dispatch) => {

        try {
            dispatch(setHomeLoading(true))
            const response = await API({
                url: `${config.API.HOME_SERVICE}/provinces`,
                method: METHOD.GET,
            })
            dispatch(getListProvincesSuccess(response));
        } catch (e) {
            dispatch(setHomeLoading(false))
            return console.error(e.message);
        }
    };

export const getHomeById = (_id) => async (dispatch) => {
    try {
        dispatch(setCurrentHomeLoading(true))
        const response = await API({
            url: config.API.HOME_SERVICE,
            method: METHOD.GET,
            params: {
                _id
            }
        })
        dispatch(getHomeSuccess(response?.[0]))
    } catch (error) {
        dispatch(setCurrentHomeLoading(false))

    }
}