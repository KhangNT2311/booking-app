import { createSlice } from "@reduxjs/toolkit";
import API from "src/api";
import config from "src/api/config";
import { METHOD } from "src/constants/enums";
const slice = createSlice({
    name: "host",
    initialState: {
        homes: [],
        home: {}
    },
    reducers: {
        getListHostHomesSuccess: (state, action) => {
            state.homes = action.payload;
        },
        getHomeSuccess: (state, action) => {
            state.home = action.payload;
        }

    },
});
export default slice.reducer;
const { getHomeSuccess, getListHostHomesSuccess } = slice.actions;
export const getListHostHomes =
    async (dispatch) => {
        try {
            const response = await API({
                url: config.API.HOME_SERVICE + "/owner",
                method: METHOD.GET,
                hasToken: true
            })

            dispatch(getListHostHomesSuccess(response));
        } catch (e) {
            return console.error(e.message);
        }
    };

export const getHomeById = (_id) => async (dispatch) => {
    try {
        const response = await API({
            url: config.API.HOME_SERVICE,
            method: METHOD.GET,
            params: {
                _id
            }
        })
        dispatch(getHomeSuccess(response?.[0]))
    } catch (error) {

    }
}