module.exports = {
    style: {
      sass:{
        loaderOptions: {
          additionalData: `
            @import "./src/styles/index.scss";
          `,
        },
      },
      postcss: {
        plugins: [
          require('tailwindcss'),
          require('autoprefixer'),
        ],
      },
    },
  }